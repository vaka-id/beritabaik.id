# Berita Baik - Webapp Portal

Setelah di clone install dulu node modules nya :

```
$ npm install
```

Kemudian install yarn dan pm2 pada node environment global :

```
$ npm install -g yarn pm2
```

Aplikasi telah siap untuk dijalankan.


## Perintah untuk menjalankan aplikasi.

### Development
Start :
```
$ yarn dev
```

Build :
```
$ yarn build
```

### Production
Start (Harus melakukan build terlebih dahulu) :
```
$ yarn start
```

Restart
```
$ yarn restart
```

Stop
```
$ yarn stop
```

### Struktur Direktori
+-- .next
|   +-- cikan