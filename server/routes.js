'use strict';

const path = require('path');
const cookieParser = require('cookie-parser');
const auth = require('./auth/auth.service');

module.exports = function (app, handle) {
  app.use(cookieParser());  

  // API
  app.use('/auth', require('./auth'));

  app.use(function (req, res, next) {
      // res.header("Access-Control-Allow-Origin", "*");
      res.header("Access-Control-Allow-Headers", "*");
      next();
  });

  const defController = (req, res, next) => {
    return handle(req, res);
  }

  //MAPPING OLD ROUTES
  app.get('/home/a/:editorialSlug/:slug', (req, res) => {
    let {editorialSlug, slug} = req.params
    res.redirect(`/read?editorialSlug=${editorialSlug}&slug=${slug}`);
  })
  app.get('/home-c/:editorialSlug/:slug', (req, res) => {
    let {editorialSlug, slug} = req.params
    res.redirect(`/read?editorialSlug=${editorialSlug}&slug=${slug}`);
  })
  app.get('/home-g/:editorialSlug/:slug', (req, res) => {
    let {editorialSlug, slug} = req.params
    res.redirect(`/read?editorialSlug=${editorialSlug}&slug=${slug}`);
  })
  app.get('/home/:editorialSlug', (req, res) => {
    let {editorialSlug} = req.params
    res.redirect(`/kanal?editorialSlug=${editorialSlug}&page=1`);
  })
  app.get('/home/:editorialType/:editorialSlug', (req, res) => {
    let {editorialSlug, editorialType} = req.params
    res.redirect(`/read?editorialSlug=${editorialSlug}&editorialType=${editorialType}&page=1`);
  })
  app.get('/home-search/s/:title', (req, res) => {
    res.redirect(`/search?title=${title}&page=1`);
  })
  app.get('/home', (req, res) => {
    res.redirect(`/`);
  })

  // PAGE ROUTES SETUP
  app.get('/', auth.checkingAuth(), defController);
  app.get('/kanal', auth.checkingAuth(), defController);
  app.get('/read', auth.checkingAuth(), defController);
  app.get('/event', auth.checkingAuth(), defController);
  app.get('/event-details', auth.checkingAuth(), defController);
  app.get('/popular', auth.checkingAuth(), defController);
  app.get('/guidelines', auth.checkingAuth(), defController);
  app.get('/about-us', auth.checkingAuth(), defController);
  app.get('/search', auth.checkingAuth(), defController);

  app.get('*', (req, res) => {
    return handle(req, res)
  })
};
