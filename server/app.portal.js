
/**
 * Created by dwiargo on 3/1/18.
 */

process.env.NODE_ENV = process.env.NODE_ENV || 'development';
process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = '0';

const body = require('body-parser');
const co = require('co');
const express = require('express');
const next = require('next');
const config = require('./config/environment');

const app = express()
const server = require('http').Server(app)
const io = require('socket.io')(server)

const dev = process.env.NODE_ENV !== 'production';
const nextApp = next({ dev });
const handle = nextApp.getRequestHandler();
const PORT = config.port;

// socket.io server
require('./config/socketio')(io);
if(dev) require('https').globalAgent.options.ca = require('ssl-root-cas/latest').create();

nextApp.prepare().then(() => {

  app.use(body.json());
  app.use('/static/images', express.static(`./uploads/images`));

  require('./routes')(app, handle);
  require('./config/express')(app, handle);  

  server.listen(PORT, (err) => {
    if (err) throw err;
    console.log(`Server ready on http://localhost:${PORT}`);
  });
});
