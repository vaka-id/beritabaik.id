'use strict';

// Development specific configuration
// ==================================
module.exports = {
  webAddress:'http://156.67.220.197:8080',
  facebook:{
    appId:'1957770944535094',
    pixelId:'facebook_pixel_id'
  },
  google:{
    trackingId:'UA-000000-01',
    clientId:'',
    clientSecret:'',
    mapKey:'AIzaSyCN0keq9G9Hby59mKdWiNWSoTERyQhwX3w',
    adsense:{
      client:'ca-pub-8951545548090902',
      slot:'5038599410',
      slotId:{
        billboard:{
          general:'2522626027'
        },
        leaderboard:{
          general:'2522626027'
        }
      }
    }
  },
  url:{
    profile:'http://156.67.220.197/settings/my-account/edit-profile',
    cms:'http://156.67.220.197/cms/dashboard'
  },
  backend:{
    host: 'https://beritabaik.id:3001',
    loginPath:'https://beritabaik.id/#/login',
    baseUrl:'/v1',
    appId:'BERITABAIK_WEBAPP_V01',
    static:{
      host:'https://beritabaik.id/cms'
    }
  },
  revive:{
    id:'51621cfdcdec7c5b878cc7235eb0223a',
    src:'//ads.beritabaik.id/www/delivery/asyncjs.php',
    zoneIds:{
      home:{
        leaderboard:'1',
        billboard:'4',
        center1:'23',
        center2:'24',
        medium1:'2',
        medium2:'3'
      },
      kanal:{
        leaderboard:'8',
        billboard:'5',
        center1:'27',
        medium1:'6',
        medium2:'7'
      },
      artikel:{
        leaderboard:'11',
        billboard:'12',
        center1:'21',
        medium1:'9',
        medium2:'10'
      }
    }
  }
};
