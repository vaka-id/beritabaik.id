
'use strict';

// Production specific configuration
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
// =================================
module.exports = {
  // Server IP
  ip:       process.env.OPENSHIFT_NODEJS_IP ||
            process.env.IP ||
            undefined,

  // Server port
  port:     process.env.OPENSHIFT_NODEJS_PORT ||
            process.env.PORT ||
            8080,

  webAddress:'https://beritabaik.id',
  facebook:{
    appId:'1957770944535094',
    pixelId:'433900747103842'
  },
  google:{
    trackingId:'UA-118969342-1',
    clientId:'41162363474-mo2568h4vs3tbs8pgepog137sbg148fa.apps.googleusercontent.com',
    clientSecret:'',
    mapKey:'AIzaSyCN0keq9G9Hby59mKdWiNWSoTERyQhwX3w',
     adsense:{
      client:'ca-pub-8951545548090902',
      slot:'5038599410',
      slotId:{
        billboard:{
          general:'2522626027'
        },
        leaderboard:{
          general:'2522626027'
        }
      }
    }
  },
  url:{
    profile:'https://beritabaik.id/cms/#/settings/my-account/edit-profile',
    cms:'https://beritabaik.id/cms/#/cms/dashboard'
  },
  backend:{
    host: 'https://beritabaik.id:3001',
    loginPath:'https://beritabaik.id/cms/#/login',
    baseUrl:'/v1',
    appId:'BERITABAIK_WEBAPP_V01',
    static:{
      host:'https://beritabaik.id/cms'
    }
  },
  revive:{
    id:'51621cfdcdec7c5b878cc7235eb0223a',
    src:'https://ads.beritabaik.id/www/delivery/asyncjs.php',
    zoneIds:{
      home:{
        leaderboard:'1',
        billboard:'4',
        center1:'23',
        center2:'24',
        medium1:'2',
        medium2:'3'
      },
      kanal:{
        leaderboard:'8',
        billboard:'5',
        center1:'27',
        medium1:'6',
        medium2:'7'
      },
      artikel:{
        leaderboard:'11',
        billboard:'12',
        center1:'21',
        medium1:'9',
        medium2:'10'
      }
  }
 }
};
