/**
 * Created by dwiargo on 4/6/16.
 */

var nodemailer = require('nodemailer');
var config = require('../config/environment');

exports.send = function (from,to,subject,message) {
    return new Promise(function (resolve, reject) {
        var transporter = nodemailer.createTransport(config.mailSender);

        transporter.sendMail({
            from: from||config.mailSender.auth.user,
            to: to,
            subject: subject,
            html: message
        }, function (err, info) {
            if(err) {
                console.log(err)
                reject(err);
            } else {
                console.log(info)            
                resolve(info)
            }
        })
    })
}
