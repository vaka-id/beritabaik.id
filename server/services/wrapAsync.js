module.exports = handler => (req, res) => handler(req, res)
  .then(result => res.json(result))
  .catch(error => res.status(500).json({ error: error.message }))
