module.exports = async (req, res, model, findOption='', query={}) => {
  let limit = req.query.limit || 20
  let page = req.query.page || 1

  if(req.query.column && req.query.keyword) {
      console.log(typeof req.query.keyword)
      if(typeof req.query.keyword === 'string'){
        let regex = '^';
        var _regex = req.query.keyword.split(' ');
        for(var i in _regex){
            regex += '(?=.*'+_regex[i]+')';
        }
        regex += '.+';
        query[req.query.column] = {$regex: new RegExp(regex,'ig')};
      } else {
        query[req.query.column] = req.query.keyword;
      }
  }

  let sort = {}
  if(req.query.sort && req.query.sortBy) {
      sort[req.query.sortBy] = Number(req.query.sort);
  } else {
    sort = {createdAt:-1}
  }

  let ne = req.query.ne;
  if(ne){
      ne = ne.split(';')
      query[ne[0]] = {$ne: ne[1]}
  }

  let tags = req.query.tags;
  if(tags){
      tags = tags.split(',')
      query.tags = {$in:tags}
  }

  let { collection } = req.params;
  let result = await model.find(query, findOption)
    .skip(Number(limit)*(Number(page) - 1))
    .limit(Number(limit))
    .sort(sort)
    
  let total = await model.count(query)
  res.setHeader('x-pagination-count', total);
  return result;
}