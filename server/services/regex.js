module.exports = (stringValue, type) => {
  let regex = '^';
  var _regex = stringValue.split(' ');
  for(var i in _regex){
      regex += '(?=.*'+_regex[i]+')';
  }
  regex += '.+';
  return new RegExp(regex,'ig');
}