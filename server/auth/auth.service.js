const config = require('../config/environment');
const qs = require('query-string');
const axios = require('axios')
const compose = require('composable-middleware')

const getUserInfo = (token, user_id) => {
  const { host, baseUrl } = config.backend;
  return axios({
    method:'get',
    url:`${host}${baseUrl}/user/info?${qs.stringify({ token, user_id })}`,
    headers:{
      ApplicationID:config.backend.appId
    }
  })
}

const checkingAuth = () => {
  return compose()
    .use(async (req, res, next) => {
      let token = req.cookies[config.cookie.token.name];
      let user_id = req.cookies['BB-UseridKey'];
      if(token && user_id){
        try{
          let userInfo = await getUserInfo(token, user_id);
          req.user = userInfo.data;
          next();
        }catch(error){
          console.warn(error);
        }
      } else {
        next();
      }
    })
}

exports.getUserInfo = getUserInfo;
exports.checkingAuth = checkingAuth;