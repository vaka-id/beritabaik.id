const express = require('express');
const router = express.Router();
const wrapAsync = require('../services/wrapAsync');
const axios = require('axios');
const qs = require('query-string');
const auth = require('./auth.service');

const config = require('../config/environment');

router.post('/login', wrapAsync(async (req, res) => {
  const { host } = config.backend;
  const { name, options } = config.cookie.token;
  const oauth = req.query.oauth;
  let targetPath = '';
  switch(oauth){
    case 'facebook':
    targetPath = '/user/fb-login';
    break;
    case 'google':
    targetPath = '/user/google-login';
    break;
    default:
    targetPath = '/user/login';
  }

  let loginRes = await axios.post(`${host}${targetPath}`, req.body);
  const { token, user_id } = loginRes.data;
  //let userInfo = await auth.getUserInfo(token, user_id)
  //req.user = userInfo.data;
  //console.log(req.user);
  res.cookie(name, token, options);
  res.cookie('BB-UseridKey', user_id, options);
  return loginRes.data;
}))

router.get('/logout', (req, res) => {
  res.clearCookie(config.cookie.token.name);
  res.clearCookie('BB-UseridKey');
  //res.redirect('/');
  return res.status(200).send('ok');
})

// https://beritabaik.id:8082/user/fb-login

module.exports = router;