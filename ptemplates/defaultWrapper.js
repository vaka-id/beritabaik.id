import React from 'react';
import { Header, Footer, Navbar, AdsLeaderboard, AdsBillboard, AdsSkinner } from '../components';
import Router from 'next/router';
import _ from 'lodash';
import config from '../server/config/environment';

const DefaultWrapper = (Children, position="Home") => {
  return class Wrapped extends React.Component{
    static async getInitialProps(ctx){
      const pageProps = Children.getInitialProps ? await Children.getInitialProps(ctx) : {};
      return pageProps;
    }

    render(){
      return(
        <div>
          <AdsLeaderboard position={position}/>
          <Header/>
          <Navbar/>
          <AdsSkinner position={position}>
            <div>
              <AdsBillboard position={position}/>
              <Children {...this.props}/>
            </div>
          </AdsSkinner>
          <Footer/>
        </div>
      )
    }
  }
}

export default DefaultWrapper;