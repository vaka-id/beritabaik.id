import React from 'react';
import { connect } from 'react-redux';
import { ArticleList, HeadlineArticle, AdsCenter, AdsMedium, InfografisPanel } from '../components';
import { Col, Row } from 'antd';
import httpService from 'golibs/services/httpService';
import config from '../server/config/environment';
import qs from 'query-string';
import _ from 'lodash';

class ArticleOthers extends React.Component{
  render(){
    const {query, mainArticles, viewUrl, hrefOption, editorialBySlug, adsPosition, showInfografis} = this.props;
    const {page, ...hrefOptionQuery} = query;
    const getMorePopularLink = () => {
      let {editorialSlug} = query;
      let url = '/popular';
      if(editorialSlug){
        if(editorialBySlug){
          url = `/popular?editorialSlug=${editorialSlug}&editorialSlugId=${editorialBySlug.id}`;
        } else {
          url = `/popular?editorialSlug=${editorialSlug}`;
        }
      }
      return url
    }

    return(
      <div>
        <div className="gn-container">
          <Row type="flex" className="gn-layout column-sm-reverse">
            <Col lg={16} md={24} sm={24} xs={24}>
              <AdsCenter 
                position={adsPosition}
                className="gn-margin-N margin-top"
              />
              <ArticleList
                className="gn-padding-N padding-right"
                title="Baca Lainnya"
                data={mainArticles ? mainArticles.data : null}
                page={mainArticles ? mainArticles.page : null}
                total={mainArticles ? mainArticles.total_entries_size : null}
                pageSize={mainArticles ? mainArticles.per_page : null}
                hrefOption={ hrefOption || {
                  url: viewUrl,
                  query:hrefOptionQuery
                }}
                fetchDataOnClient={ mainArticles ? null : async (pquery, callback) => {
                  const { host, baseUrl } = config.backend;
                  try{
                    let res = await httpService.get(`${host}${baseUrl}/articles-editorial/${query.editorialSlug}?${qs.stringify(_.merge( editorialBySlug ? {
                      editorialType: 'p',
                      editorialSlugID: editorialBySlug.id
                    } : {} , {
                      editorialSlug:query.editorialSlug,
                      page: pquery.page,
                      per_page: 10,
                      type:"news"
                    }))}`)
                    callback(res.data.data, res.data.total_entries_size);
                  }catch(error){
                    callback(null, null, true, error.message);
                  }
                }}
              />
            </Col>
            <Col lg={8} md={24} sm={24} xs={24}>
              <AdsMedium
                position={adsPosition}
                index={1}
                className="gn-margin-N margin-top"
              />
              <ArticleList
                title="Berita Baik Terpopuler"
                moreLink={getMorePopularLink()}
                isVerticalMode={true}
                showActions={false}
                fetchDataOnClient={ async (pquery, callback) => {
                  const { host, baseUrl } = config.backend;
                  try{
                    let res = await httpService.get(`${host}${baseUrl}/article/popular?${qs.stringify(_.merge( editorialBySlug ? {
                      editorialType: 'p',
                      editorialSlugID: editorialBySlug.id
                    } : {} , {
                      editorialSlug:query.editorialSlug,
                      page: pquery.page,
                      per_page: 5,
                      type:"news"
                    }))}`)
                    callback(res.data.data, res.data.total_entries_size < 5 ? res.data.total_entries_size : 5);
                  }catch(error){
                    callback(null, null, true, error.message);
                  }
                }}
              />
              {showInfografis ? <InfografisPanel/> : null}
              <AdsMedium
                position={adsPosition}
                index={2}
                className="gn-margin-N margin-top margin-bottom"
              />
            </Col>
          </Row>
        </div>
      </div>
    )
  }
}

ArticleOthers.defaultProps = {
  showInfografis: false
}

export default connect(state => ({
  query: state.query
}))(ArticleOthers);