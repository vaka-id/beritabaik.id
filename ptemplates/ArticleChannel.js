import React from 'react';
import { connect } from 'react-redux';
import { ArticleList, HeadlineArticle, ArticleDetails } from '../components';
import ArticleOthers from './ArticleOthers';
import { Col, Row } from 'antd';
import httpService from 'golibs/services/httpService';
import config from '../server/config/environment';
import qs from 'query-string';

class ArticleChannel extends React.Component{
  render(){
    const { query, latestArticle, articleImageData, ...articleOthersProps } = this.props;
    return(
      <div>
        {query.editorialSlug === 'gallery-foto' ? (
          <ArticleDetails article={latestArticle[0]} images={articleImageData}/>
        ) : (
          <HeadlineArticle className="padding-top" data={latestArticle }/>
        )}
        <ArticleOthers {...articleOthersProps}/>
      </div>
    )
  }
}

export default connect( state => {
  const { query } = state;
  return { query };
})(ArticleChannel);