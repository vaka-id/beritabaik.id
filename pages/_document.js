import React from 'react';
import Document, { Main, NextScript } from 'next/document';
import Meta from '../golibs/Meta';
import enUS from "antd/lib/locale-provider/en_US";
import { LocaleProvider } from "antd";
import flush from "styled-jsx/server";

class Page extends Document {
  static getInitialProps({ renderPage }) {
    const { html, head, errorHtml, chunks } = renderPage();
    const styles = flush(); 
    return { html, head, errorHtml, chunks, styles };
  }

  render() {
    return (
      <html lang="en">
        <Meta title="Berita Baik | Beritabaik.id"/>
        <body>
          <LocaleProvider locale={enUS}>
            <Main />
          </LocaleProvider>
          <NextScript />
        </body>
      </html>
    );
  }
}

export default Page;
