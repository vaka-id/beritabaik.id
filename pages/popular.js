import React from 'react';
import { connect } from 'react-redux';
import { defaultWrapper } from '../ptemplates';
import { ArticleList } from '../components';
import httpService from 'golibs/services/httpService';
import { SetTitle } from '../golibs/structures/Set';
import qs from 'query-string';

class Popular extends React.Component{
  render(){
    const { query, backendConf } = this.props;
    return(
      <div className="gn-container">
        <SetTitle title="Beranda | Beritabaik.id"/>
        <ArticleList
          className="gn-margin-L margin-top"
          title={<h2>Berita Baik Terpopuler</h2>}
          defaultCol={8}
          fetchDataOnClient={ async (pquery, callback) => {
            const { host, baseUrl } = backendConf;
            const { editorialSlug, editorialSlugId } = query;
            try{
              let res = await httpService.get(`${host}${baseUrl}/article/popular?${qs.stringify(_.merge( editorialSlugId ? {
                editorialType: 'p',
                editorialSlugID: editorialSlugId
              } : {} , {
                editorialSlug:editorialSlug,
                page: pquery.page,
                per_page: 5,
                type:"news"
              }))}`)
              callback(res.data.data, res.data.total_entries_size < 5 ? res.data.total_entries_size : 5);
            }catch(error){
              callback(null, null, true, error.message);
            }
          }}
        />
      </div>
    )
  }
}

export default defaultWrapper(connect(
  state => ({
    query: state.query,
    backendConf:state.backendConf
  })
)(Popular), 'Popular');