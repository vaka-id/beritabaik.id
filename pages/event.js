import React from 'react';
import { defaultWrapper } from '../ptemplates';
import { SubscriptionPanel, CalendarEvent } from '../components';
import httpService from 'golibs/services/httpService';
import { SetTitle } from '../golibs/structures/Set';
import qs from 'query-string';

const Event = ({today, premiumEvents}) => {
  return(
    <div>
      <SetTitle title="Beranda | Beritabaik.id"/>
      <CalendarEvent 
        today={today} 
        premiumEvents={premiumEvents}
      />
      <SubscriptionPanel/>
    </div>
  )
}

Event.getInitialProps = async ({req}) => {
  const { host, baseUrl } = req.backendConf;
  try{
    let premiumEventRes = await httpService.get(`${host}${baseUrl}/events-premium?${qs.stringify({
      page:1,
      per_page:5
    })}`)
    return {
      today: new Date(),
      premiumEvents:premiumEventRes.data
    }

  }catch(error){
    console.warn(error)
  }
}

export default defaultWrapper(Event, 'Artikel');