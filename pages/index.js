import React from 'react';
import { 
  Header, Footer, HeadlineArticle, HeadlineCategory, HeadlineEvent,
  HeadlineGallery, HeadlineVideo, ArticleList, Billboard, SubscriptionPanel,
  InfografisPanel, AdsCenter, AdsMedium, Separator
} from '../components';
import { Row, Col } from 'antd';
import httpService from 'golibs/services/httpService';
import config from '../server/config/environment/index';
import Navbar from 'components/Header/Navbar';
import qs from 'query-string';
import { defaultWrapper } from '../ptemplates';
import { Set, SetTitle, setArticle, SetMeta } from '../golibs/structures/Set';

const GroupingContent = ({beritaTerbaru, beritaPopuler, indonesiaMembangun, indonesiaBangga}) => {
  return(
    <div>
      <div className="gn-container">
        <Row type="flex">
          <Col lg={16} md={24} sm={24} xs={24}>
            <ArticleList
              className="gn-padding-N padding-right"
              title="Berita Baik Terbaru"
              moreLink="/kanal?editorialSlug=indonesia-baik&page=1"
              data={beritaTerbaru.data}
            />
            <AdsCenter 
              position="Home"
              className="gn-margin-N margin-top"
              index={1}
            />
            <ArticleList
              className="gn-padding-N padding-right"
              title="Indonesia Bangga"
              moreLink="/kanal?editorialSlug=indonesia-bangga&editorialType=p&page=1"
              data={indonesiaBangga.data}
            />
            <ArticleList
              className="gn-padding-N padding-right"
              title="Indonesia Membangun"
              moreLink="/kanal?editorialSlug=indonesia-membangun&page=1"
              data={indonesiaMembangun.data}
            />
          </Col>
          <Col lg={8} md={24} sm={24} xs={24}>
            <AdsMedium
              position="Home"
              className="gn-margin-N margin-top"
              index={1}
            />
            {beritaPopuler.data.length > 0 ? (
              <div>
                <ArticleList
                  title="Berita Baik Terpopuler"
                  moreLink="/popular"
                  data={beritaPopuler.data}
                  isVerticalMode={true}
                  showActions={false}
                />
                <InfografisPanel/>
              </div>
            ) : null }
            <AdsMedium
              position="Home"
              className="gn-margin-N margin-top"
              index={2}
            />
          </Col>
        </Row>
        <AdsCenter 
          position="Home"
          className="gn-margin-N margin-top margin-bottom"
          index={2}
        />
      </div>
    </div>
  )
}

const Index = ({...articlesData}) => {
  return (
    <div>
      <SetTitle title="Beranda | Beritabaik.id"/>
      <SetMeta name="language" content="id"/>
      <SetMeta name="robots" content="index, follow"/>
      <SetMeta name="googlebot" content="index, follow"/>
      <SetMeta name="description" content="Beritabaik.id adalah portal berita yang mengusung gaya positive journalism, yang lebih banyak membicarakan tentang prestasi dan kebaikan-kebaikan skala besar maupun kecil dari seluruh penjuru nusantara"/>
      <SetMeta property="og:type" content="article"/>
      <SetMeta property="og:url" content="https://beritabaik.id"/>
      <SetMeta property="og:title" content="Beritabaik.id"/>
      <SetMeta property="og:image" content={`${config.webAddress}/static/images/logo-berita-baik.png`}/>
      <SetMeta property="og:site_name" content="Beritabaik.id"/>
      <SetMeta property="og:description" content="Beritabaik.id adalah portal berita yang mengusung gaya positive journalism, yang lebih banyak membicarakan tentang prestasi dan kebaikan-kebaikan skala besar maupun kecil dari seluruh penjuru nusantara"/>
      <SetMeta property="fb:app_id" content={config.facebook.appId}/>
      <SetMeta property="twitter:card" content="summary_large_image"/>
      <SetMeta property="twitter:site" concent="@beritabaik_id"/>
      <SetMeta property="twitter:description" content="Beritabaik.id adalah portal berita yang mengusung gaya positive journalism, yang lebih banyak membicarakan tentang prestasi dan kebaikan-kebaikan skala besar maupun kecil dari seluruh penjuru nusantara"/>
      <SetMeta property="twitter:creator" content="@beritabaik_id"/>
      <SetMeta property="twitter:title" content="Beritabaik.id"/>

      <HeadlineCategory/>
      <HeadlineArticle/>
      <GroupingContent {...articlesData}/>
      <HeadlineGallery/>
      {/* <HeadlineVideo/> */}
      <AdsCenter 
          position="Home"
          className="gn-margin-N margin-top margin-bottom"
          index={3}
        />
      <HeadlineEvent/>
      <SubscriptionPanel/>
      <AdsCenter 
          position="Home"
          className="gn-margin-N margin-top margin-bottom"
          index={4}
        />
    </div>
  );
}

Index.getInitialProps = async () => {
  let { host, baseUrl } = config.backend;
  try{
    let beritaTerbaruRes = await httpService.get(`${host}${baseUrl}/articles-all/latest?${qs.stringify({
      page:1,
      type:'news',
      per_page:4
    })}`);
    let beritaPopulerRes = await httpService.get(`${host}${baseUrl}/article/popular?${qs.stringify({
      page:1,
      per_page: 6
    })}`)
    let indonesiaBanggaSlug = await httpService.get(`${host}${baseUrl}/editorial-by-slug/indonesia-bangga`)
    let indonesiaBanggaRes = await httpService.get(`${host}${baseUrl}/articles-editorial/indonesia-bangga?${qs.stringify({
      editorialSlug:'indonesia-bangga',
      editorialType:'p',
      editorialSlugID:indonesiaBanggaSlug.data.id,
      page: 1,
      per_page: 2,
      type:'news'
    })}`)
    let indonesiaMembangunRes = await httpService.get(`${host}${baseUrl}/articles-editorial/indonesia-membangun?${qs.stringify({
      editorialSlug:'indonesia-membangun',
      page: 1,
      per_page: 2,
      type:'news'
    })}`)
    return {
      beritaTerbaru:beritaTerbaruRes.data,
      beritaPopuler:beritaPopulerRes.data,
      indonesiaBangga:indonesiaBanggaRes.data,
      indonesiaMembangun:indonesiaMembangunRes.data
    }
  } catch(error){
    console.log(error);
  }
}

export default defaultWrapper(Index, "Home");
