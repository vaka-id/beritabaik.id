import React from 'react';
import { defaultWrapper } from '../ptemplates';
import { SubscriptionPanel, CalendarEvent } from '../components';
import httpService from 'golibs/services/httpService';
import qs from 'query-string';
import { SetTitle } from '../golibs/structures/Set';

const { Details } = CalendarEvent;

const EventDetails = (props) => (
  <div>
    <SetTitle title="Beranda | Beritabaik.id"/>
    <Details {...props}/>
    <SubscriptionPanel/>
  </div>
)

EventDetails.getInitialProps = async ({req}) => {
  const { host, baseUrl } = req.backendConf;
  const { query } = req;
  
  try{
     let premiumEventRes = await httpService.get(`${host}${baseUrl}/events-premium?${qs.stringify({
      page:1,
      per_page:5
    })}`)

    let eventRes = await httpService.get(`${host}${baseUrl}/events/${query.id}`)
    return {
      today: new Date(),
      event:eventRes.data,
      premiumEvents: premiumEventRes.data
    }

  }catch(error){
    console.warn(error)
  }
}

export default defaultWrapper(EventDetails, 'Artikel');