import React from 'react';
import { defaultWrapper } from '../ptemplates';
import httpService from 'golibs/services/httpService'
import { ArticleList, SearchPanel } from '../components';
import qs from 'query-string';
import config from '../server/config/environment';
import _ from 'lodash';
import { SetTitle } from 'golibs/structures/Set';

const Search = ({mainArticles, query, backendConf, defaultQueryParameter, isSearchAdvance}) => (
  <div className="gn-grey-background">
    <SetTitle title="Beranda | Beritabaik.id"/>
    <div className="gn-container">
      <SearchPanel 
        defaultQuery={query} 
        backendConf={backendConf}
        total={mainArticles ? mainArticles.total_entries_size : 0}
      />
      <ArticleList
        className="gn-padding-N padding-right"
        defaultCol={8}
        // data={mainArticles ? mainArticles.data : null}
        // page={mainArticles ? mainArticles.page : null}
        // total={mainArticles ? mainArticles.total_entries_size : null}
        // pageSize={mainArticles ? mainArticles.per_page : null}
        // hrefOption={{
        //   url: '/search',
        //   query:{}
        // }}
        articleCardStyle={{
          padding: 8
        }}
        fetchDataOnClient={ async (pquery, callback) => {
          const { host, baseUrl } = config.backend;
          let queryParameter = _.merge(defaultQueryParameter, {
            page:pquery.page || 1,
          })
          try{
            let res = await httpService.get(`${host}${baseUrl}/${isSearchAdvance ? 'articles-search-advanced' : 'articles-search'}?${qs.stringify(queryParameter)}`)
            callback(res.data.data, res.data.total_entries_size);
          }catch(error){
            callback(null, null, true, error.message);
          }
        }}
      />
    </div>
  </div>
)

Search.getInitialProps = async ({req}) => {
  const { query, backendConf } = req;
  const { host, baseUrl } = backendConf;
  
  const {title, reporter_name, start_date, end_date} = query;
  let isSearchAdvance = false;
  try{
    let queryParameter = {
      page:query.page || 1,
    };
    let defaultQueryParameter = {
      per_page:query.per_page || 9,
      article_type:'news'
    };

    if(start_date && end_date){
      defaultQueryParameter.reporter_name = reporter_name;
      defaultQueryParameter.start_date = start_date;
      defaultQueryParameter.end_date = end_date;
      isSearchAdvance = true;
    } else {
      defaultQueryParameter.title = title;
    }

    queryParameter = _.merge(queryParameter, defaultQueryParameter);
    let res = await httpService.get(`${host}${baseUrl}/${isSearchAdvance ? 'articles-search-advanced' : 'articles-search'}?${qs.stringify(queryParameter)}`)
    
    return{
      query,
      defaultQueryParameter,
      isSearchAdvance, 
      backendConf,
      mainArticles:res.data
    }
  } catch(error){
    console.warn(error)
  }
}

export default defaultWrapper(Search, 'Search');