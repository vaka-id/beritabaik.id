import React from 'react';
import { ContentAbout } from '../components';
import { defaultWrapper } from '../ptemplates';
import { SetTitle } from '../golibs/structures/Set';

class ContactUs extends React.Component{
  render(){
    return(
      <div>
        <SetTitle title="Beranda | Beritabaik.id"/>
        <ContentAbout 
          showTeam={false}
          showAbout={false}
        />
      </div>
    )
  }
}

export default defaultWrapper(ContactUs, 'Contact')