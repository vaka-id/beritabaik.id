import React from 'react';
import { ContentGuidelines } from '../components';
import { defaultWrapper } from '../ptemplates';
import { SetTitle } from '../golibs/structures/Set';

class Guidelines extends React.Component{
  render(){
    return(
      <div>
        <SetTitle title="Beranda | Beritabaik.id"/>
        <div className="gn-container">
          <ContentGuidelines/>
        </div>
      </div>
    )
  }
}

export default defaultWrapper(Guidelines, 'Guidelines')