import React from 'react';
import { Header, Footer, Navbar, ArticleDetails, HeadlineArticle } from '../components';
import { defaultWrapper, ArticleChannel, ArticleOthers } from '../ptemplates';
import config from '../server/config/environment';
import httpService from 'golibs/services/httpService';
import qs from 'qs';
import { SetTitle } from '../golibs/structures/Set';
import _ from 'lodash';

const getLatestComponent = (editorialSlug, latestArticle, articleImageData) => {
  switch(editorialSlug){
    case 'foto-kamu':
    case 'video':
    case 'gallery-foto':
    case 'infografis':
    return latestArticle[0] !== "" ? <ArticleDetails article={latestArticle[0]} images={articleImageData} videoHeight={380}/> : null;
    default:
    return <HeadlineArticle className="padding-top" data={latestArticle }/>
  }
}

const Kanal = ({ query, latestArticle, articleImageData, ...articleOthersProps }) => (
  // <div>
  //   <ArticleChannel 
  //     {...props}
  //     viewUrl="/kanal"
  //   />
  // </div>
  <div>
    {/* {['gallery-foto', 'infografis'].indexOf(query.editorialSlug) >= 0 ? (x
      <ArticleDetails article={latestArticle[0]} images={articleImageData}/>
    ) : (
      <HeadlineArticle className="padding-top" data={latestArticle }/>
    )} */}
    <SetTitle title={`${query.editorialSlug} | Beritabaik.id`}/>
    {getLatestComponent(query.editorialSlug, latestArticle, articleImageData)}
    <ArticleOthers 
      // adsPosition={['indonesia-baik'].indexOf(query.editorialSlug) >= 0 ? 'Kanal' : null}
      adsPosition="Kanal"
      {...articleOthersProps} viewUrl='/kanal'
      showInfografis={['video', 'gallery-foto'].indexOf(query.editorialSlug) >= 0 ? true : false}
    />
  </div>
)

Kanal.getInitialProps = async ({req}) => {
  const { host, baseUrl } = config.backend;
  const { query } = req
  const { editorialSlug, editorialType, type='news' } = query;
  
  let editorialBySlug;
  let articleImageRes; 
  let params = {
    editorialSlug, type
  }

  try{
    const isVarious = editorialType === 'p';
    const isGallery = new Array('gallery-foto').indexOf(query.editorialSlug) >= 0 ? true : false;

    let latestArticleRes;
    if(isVarious){
      editorialBySlug = await httpService.get(`${host}${baseUrl}/editorial-by-slug/${editorialSlug}`)
      editorialBySlug = editorialBySlug.data
      params = _.merge(params, {
        editorialType, editorialSlugID:editorialBySlug.id
      })
    } else {
      latestArticleRes = await httpService.get(`${host}${baseUrl}/articles-editorial/latest/${editorialSlug}?${qs.stringify({
        editorialSlug:req.query.editorialSlug,
        type:'news'
      })}`)
    }

    if(isGallery){
      articleImageRes = await httpService.get(`${host}${baseUrl}/article-image-by-article/${
        isVarious ? mainArticlesRes.data.data[0].id :latestArticleRes.data.id
      }`)
    }

    let mainArticlesRes = await await httpService.get(`${host}${baseUrl}/articles-editorial/${query.editorialSlug}?${qs.stringify(_.merge( params, {
      page: query.page,
      per_page: 20,
    }))}`)

    return {
      query: query,
      editorialBySlug, editorialSlug,
      latestArticle: new Array(isVarious ? mainArticlesRes.data.data[0] : latestArticleRes.data),
      mainArticles:mainArticlesRes.data,
      articleImageData: isGallery ? articleImageRes.data : null
    }
  }catch(error){
    console.warn(error);
  }
}

export default defaultWrapper(Kanal, "Kanal");