import React from 'react';
import App, {Container} from 'next/app';
import { Provider } from 'react-redux';
import moment from 'moment';
import config from '../server/config/environment';
moment.locale('id');

import { initializeStore, setProperties } from '../redux/store';
import withRedux from 'next-redux-wrapper'; 
import { fbp, ga } from '../golibs';
import { initRevive } from 'components/Ads';
/**
* @param {object} initialState
* @param {boolean} options.isServer indicates whether it is a server side or client side
* @param {Request} options.req NodeJS Request object (not set when client applies initialState from server)
* @param {Request} options.res NodeJS Request object (not set when client applies initialState from server)
* @param {boolean} options.debug User-defined debug mode param
* @param {string} options.storeKey This key will be used to preserve store in global namespace for safe HMR 
*/

class MyApp extends App {
  static async getInitialProps({Component, ctx}) {
    const {req} = ctx;
    if(req){
      req.backendConf = config.backend;
      ctx.store.dispatch(setProperties({
        user: req.user || null,
        query: req.query,
        backendConf:config.backend,
        webAddress: config.webAddress,
        facebook: config.facebook,
        google: config.google,
        url: config.url
      }));
    }
    
    const pageProps = Component.getInitialProps ? await Component.getInitialProps(ctx) : {};
    moment.locale('id');
    return {pageProps};
  }

  componentDidMount(){
    fbp.init(config.facebook.pixelId);
    fbp.pageView();
    ga.init(config.google.trackingId);
    ga.pageView(window.location.href);
    setTimeout(() => initRevive(config.revive.src), 500);
  }

  render() {
    const {Component, pageProps, store} = this.props;
    return (
      <Container>
        <Provider store={store}>
          <Component {...pageProps} />
        </Provider>
      </Container>
    );
  }
}


export default withRedux(initializeStore, {debug:false})(MyApp);
