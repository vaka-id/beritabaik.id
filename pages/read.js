import React from 'react';
import { ArticleDetails } from '../components';
import { defaultWrapper, ArticleOthers } from '../ptemplates';
import config from '../server/config/environment';
import httpService from 'golibs/services/httpService';
import { Set, SetTitle, setArticle, SetMeta } from '../golibs/structures/Set';
import { fbp, ga } from 'golibs';
import Router from 'next/router';

class Read extends React.Component{
  componentDidMount(){
    const { articleData } = this.props;
    fbp.viewContent({
      content_name: articleData.title,
      content_category: articleData.slug
    })
    ga.event({
      category:'User',
      action:'Read an Article',
      label: articleData.slug
    })
    document.title = "Amazing Page"//articleData.title
  }

  render(){
    const { articleData, articleImageData} = this.props;
    const image = (articleImageData ? `${config.backend.static.host}${articleImageData[0].url}` : `${config.backend.static.host}${articleData.main_image}`).replace(/ /g, '%20');
    const pageUrl = `${config.webAddress}/read?editorialSlug=${articleData.editorial.slug}&slug=${articleData.slug}`
    return(
      <div>
        <SetTitle title={`${articleData.title} | Beritabaik.id`}/>
        <SetMeta name="description" content={articleData.teaser}/>
        <SetMeta name="keywords" content={articleData.article_tags}/>
        <SetMeta name="author" content={articleData.reporter_name}/>

        {/* Google */}
        <SetMeta itemProp="name" content={articleData.title}/>
        <SetMeta itemProp="description" content={articleData.teaser} />
        <SetMeta itemProp="image" content={image} />

        {/* Facebook */}
        <SetMeta property="fb:app_id" content={config.facebook.appId}/>

        {/* OpenGraph */}
        <SetMeta property="og:url" content={pageUrl}/>
        <SetMeta property="og:type" content="website"/>
        <SetMeta property="og:title" content={articleData.title}/>
        <SetMeta property="og:description" content={articleData.teaser}/>
        <SetMeta property="og:image" content={image}/>
        <SetMeta property="og:locale" content="id_ID"/>
        <SetMeta property="og:image:width" content="600"/>
        <SetMeta property="og:image:height" content="315"/>
        <SetMeta rel="canonical" href={pageUrl}/>

        {/* Twitter */}
        <SetMeta property="twitter:card" content="summary_large_image"/>
        <SetMeta property="twitter:title" content={articleData.title}/>
        <SetMeta property="twitter:description" content={articleData.teaser}/>
        <SetMeta property="twitter:image" content={image}/>
        <SetMeta property="twitter:site" concent="@beritabaik_id"/>
        <SetMeta property="twitter:creator" concent="@beritabaik_id"/>

        {/* Structure */}
        <Set
          ld={setArticle(
            articleData.title,
            [`${config.backend.static.host}${articleData.main_image}`],
            articleData.publish_date,
            articleData.updated_at,
            articleData.reporter_name,
            articleData.teaser
          )}
        />
        <ArticleDetails article={articleData} images={articleImageData}/>
        <ArticleOthers showInfografis adsPosition="Artikel"/>
      </div>
    )
  }
}

Read.getInitialProps = async ({req}) => {
  const { host, baseUrl } = config.backend;
  const { query } = req;
  let articleImageRes;

  try{
    const isGallery = new Array('gallery-foto').indexOf(query.editorialSlug) >= 0 ? true : false;
    let articleRes = await httpService.get(`${host}${baseUrl}/article-by-slug/${req.query.slug}`);
    if(isGallery){
      articleImageRes = await httpService.get(`${host}${baseUrl}/article-image-by-article/${articleRes.data.id}`)
    }
    
    return {
      articleData: articleRes.data,
      articleImageData: isGallery ? articleImageRes.data : null
    }
  }catch(error){
    console.warn(error);
    Router.push('/');
    // return {};
  }
}

export default defaultWrapper(Read, 'Artikel');