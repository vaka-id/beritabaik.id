import React from 'react';
import { ContentAbout } from '../components';
import { defaultWrapper } from '../ptemplates';
import { SetTitle } from '../golibs/structures/Set';

class AboutUs extends React.Component{
  render(){
    return(
      <div>
        <SetTitle title="Beranda | Beritabaik.id"/>
        <ContentAbout/>
      </div>
    )
  }
}

export default defaultWrapper(AboutUs, 'About')