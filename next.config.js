const withSass = require('@zeit/next-sass')
const path = require('path');
const withTypescript = require('@zeit/next-typescript')
// const ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = withTypescript(withSass({
  webpack (config, options) {
    config.module.rules.push(
      {
        test: /\.(png|jpg|gif|svg|eot|ttf|woff|woff2)$/,
        use: {
          loader: 'url-loader',
          options: {
            limit: 100000
          }
        }
      },
      {
        test: /\.(less)/,
        loader: "emit-file-loader",
        options: {
          name: "dist/[path][name].[ext]"
        }
      },
      {
        test: /\.less$/,
        use: [
          {loader:"babel-loader"}, 
          {loader:"raw-loader"}, 
          {
            loader:"less-loader",
            options:{
              javascriptEnabled:true
            }
          }
        ]

      }
      /* SETTING FOR ELEMENT-REACT (ELEMENT-UI)
      {
        test: /\.(js|jsx)$/,
        loader: 'babel-loader',
        include:[
          path.resolve(__dirname, './node_modules/element-react')
        ],
      },
      { test: /\.css$/, use: ExtractTextPlugin.extract({ fallback: 'style-loader', use: [ 'css-loader' ] }) }
      */
    )
    /* SETTING FOR ELEMENT-REACT (ELEMENT-UI)
    config.plugins.push(
      new ExtractTextPlugin('style.css')
    )
    */
    return config
  }
}))