import ContentAbout from './About';
import ContentGuidelines from './Guidelines';

export {
  ContentAbout,
  ContentGuidelines
}