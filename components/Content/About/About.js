import React from 'react';
import { connect } from 'react-redux';
import { MediaImage } from '../../Media';
import { Row, Col, Button } from 'antd';
import {Gmaps, Marker, InfoWindow, Circle} from 'react-gmaps';
import { isMobile } from 'react-device-detect';


import '../Content.scss';
import './About.scss';

const teams = [
  {
    name:'Avitia Nurmatari',
    position:'Chief Executive Officer',
  },
  {
    name:'Irfan Nasution',
    position:'Managing Editor'
  },
  {
    name:'Teguh Kusumah Akbar',
    position:'Head Creative'
  },
  {
    name:'Winda Yuliani',
    position:'Marketing Communication'
  },
  {
    name:'Djuli Pamungkas',
    position:'Photographer'
  },
  {
    name:'Agam',
    position:'Graphic Design'
  },  
  {
    name:'Marissa Putri',
    position:'Human Resource Department'
  },
  {
    name:'Puti Cinintya',
    position:'Social Media Strategist'
  },
  {
    name:'Oris Riswan',
    position:'Reporter'
  },
  {
    name:'Rayhadi',
    position:'Reporter'
  }
]

const coords = {
  lat: -6.870953,
  lng: 107.622456
};

const email = {
  redaksi:'redaksi@beritabaik.id',
  partnership:'partnership@beritabaik.id'
}

class ContentAbout extends React.Component{
  state = {
    mapHeight:'480px'
  }
  componentDidMount(){
    if(isMobile) this.setState({mapHeight: '280px'})
  }

  render(){
    const { showAbout, showContact, showMap, showTeam } = this.props;
    const AboutUs = (
      <div className="about-us gn-container gn-padding-L padding-top padding-bottom">
        <h1 className="font-secondary gn-primary-color">Tentang Kami</h1>
        <p>Indonesia adalah negara yang kaya dan ramah. Sepenggal kalimat ini menunjukkan bahwa negeri ini patut untuk dibanggakan di mata rakyatnya sendiri. Dengan semangat ini, <b>beritabaik.id</b> lahir untuk mengapresiasi segala macam kebaikan dan prestasi Indonesia sekecil apapun.</p>
        <p>Di bawah PT. Warta Milenial Indonesia (WMI), Beritabaik.id adalah portal berita yang mengusung gaya positive journalism. Informasi yang kami suguhkan lebih banyak membicarakan tentang prestasi dan kebaikan-kebaikan skala besar maupun kecil dari seluruh penjuru nusantara. Karena kami yakin dan percaya, masih banyak hal-hal positif di negeri ini yang belum terberitakan.</p>
        <p>Dengan konten-konten informasi bernada positif, <b>beritabaik.id</b> tidak bermaksud untuk menghilangkan sisi kritis masyarakat. Kehadiran kami justru ingin menjadi penyeimbang sekaligus penyegar informasi di tengah kejenuhan masyarakat terhadap jumlah berita bernada negatif di sejumlah media massa ternama di Indonesia yang masih mendominasi arus informasi publik.</p>
        <p>Beritabaik.id memiliki visi untuk menjadi media massa nasional yang secara khusus mengangkat isu-isu positif di seluruh penjuru Indonesia, serta mengubah pola pikir skeptis dan pratirasa terhadap kondisi sosial, ekonomi hingga politik di Indonesia, khususnya pada level generasi milenial dan generasi Z, agar generasi berikutnya memiliki cara pandang yang lebih positif.</p>
        <p>Mari berbagi kebaikan bersama Beritabaik.id. Karena kebaikan sekecil apapun menjadi lebih berarti ketika dihargai</p>
      </div>
    );

    const OurTeams = (
      <div className="our-teams gn-grey-background gn-padding-L padding-top padding-bottom">
        <div className="gn-container">
          <h1 className="font-secondary gn-primary-color">Tim Kami</h1>
          <Row type="flex">
            {teams.map((d, i) => (
              <Col lg={6} md={6} sm={12} xs={12}>
                <div className="team-item">
                  <div className="photo">
                    <MediaImage 
                      src={`/static/images/teams/${d.name}.png`} alt={d.name}
                      style={{
                        width: '100%',
                        height: '100%',
                        borderRadius: '50%',
                        border: 'none',
                        background:'transparent'
                      }}
                    />
                  </div>
                  <div className="info gn-margin-N margin-top">
                    <div className="name">{d.name}</div>
                    <div>{d.position}</div>
                  </div>
                </div>
              </Col>
            ))}
          </Row>
        </div>
      </div>
    )

    const ContactUs = (
      <div className="contact-us gn-container gn-padding-L padding-top padding-bottom">
        <h1 className="font-secondary gn-primary-color">Hubungi Kami</h1>
        <Row type="flex">
          <Col lg={8} md={8} sm={12} xs={24}>
            <h2 className="font-secondary">Bandung</h2>
            <p>
              Jl. Cigadung Raya Barat no. 5<br/>
              Bandung, Jawa Barat, 40191<br/>
              Indonesia
            </p>
          </Col>
          <Col lg={16} md={16} sm={12} xs={24}>
            <h2 className="font-secondary">Redaksi</h2>
            <p><a href={`mailto:${email.redaksi}`}>{email.redaksi}</a></p>
            <h2 className="font-secondary">Bisnis dan Kerjasama</h2>
            <p><a href={`mailto:${email.partnership}`}>{email.partnership}</a></p>
          </Col>
        </Row>
        <a href={`mailto:${email.redaksi}`}>
          <Button type="primary">Kirim kami email</Button>
        </a>
      </div>
    )

    const Map = (
      <div className="gn-container gn-margin-L margin-top margin-bottom">
        <h1 className="font-secondary gn-primary-color">Peta Lokasi</h1>
        <Gmaps
          width={'100%'}
          height={this.state.mapHeight}
          lat={coords.lat}
          lng={coords.lng}
          zoom={16}
          loadingMessage={'loding map...'}
          mapTypeControl={true}
          scaleControl={true}
          streetViewControl={true}
          rotateControl={true}
          fullscreenControl={true}
          params={{
            v: '3.exp',
            key: this.props.google.mapKey
          }}
          onMapCreated={(map) => {
            map.setOptions({
              disableDefaultUI: true
            });
          }}>
          <Marker
            lat={coords.lat}
            lng={coords.lng}
            draggable={true}
            onDragEnd={() => {}} />
          <InfoWindow
            lat={coords.lat}
            lng={coords.lng}
            content={'Beritabaik.id)'}
            onCloseClick={() => {}} />
        </Gmaps>
      </div>
    )

    return(
      <div className="bb-content bb-content-about">
        { showAbout ? 
          <div>
            <MediaImage
              src={`/static/images/us.jpg`}
              alt="about-beritabaik.id"
            />
            {AboutUs}
            </div>
        : null}
        { showTeam ? OurTeams : null }
        { showContact ? ContactUs : null }
        { showMap ? Map : null}
      </div>
    )
  }
}

ContentAbout.defaultProps = {
  showTeam: true,
  showAbout: true,
  showContact: true,
  showMap: true
}

export default connect( state => ({
  google: state.google
}))(ContentAbout);