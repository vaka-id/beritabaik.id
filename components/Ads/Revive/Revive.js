import React from 'react';
import config from '../../../server/config/environment';
import './Revive.scss';

const Revive = ({position, type, className=''}) => {
  let conf = config.revive.zoneIds[String(position).toLowerCase()];

  return conf ? (
    <div className={`bb-ads-revive bb-ads-${String(position).toLowerCase()}-revive ${className}`}>
      <ins data-revive-zoneid={conf[type]} data-revive-id={config.revive.id}></ins>
    </div>
  ) : null
}

Revive.types = {
  LEADERBOARD:'leaderboard',
  BILLBOARD:'billboard',
  MEDIUM1:'medium1',
  MEDIUM2:'medium2',
  CENTER1:'center1',
  CENTER2:'center2'
};

export default Revive;