import React from 'react';
import { connect } from 'react-redux';
import { MediaImage } from '../../Media';
import './MediumRevive.scss';

class MediumRevive extends React.Component{
  state = {
    show: true
  }

  componentDidMount(){
    this.init();
  }

  init = async () => {
    const { host, baseUrl } = this.props.backendConf;
    const { position, index, query } = this.props;
    console.log('hallo!')
    try{
      setTimeout(this.initElement(), 500)
    }catch(error){
      console.log(error);
    }
  }
  initElement(){
    const elem = document.createElement("script");
    elem.src = "http://156.67.220.197/revive/www/delivery/asyncjs.php";
    elem.async = true;
    elem.defer = true;
    document.body.insertBefore(elem, document.body.firstChild);
  }

  render(){
    const { data, show } = this.state;
    const {style, className, google} = this.props;
    return(
      <div className="gn-margin-N margin-top bb-ads-medium" style={style}>
        <ins data-revive-zoneid="2" data-revive-id="ef3931656b827192976175c6ec5445e5"></ins>
      </div>
    )
  }
}

MediumRevive.defaultProps = {
  position: 'Home',
  style:{},
  className:'',
  index: 1
}

export default connect(state => ({
  backendConf:state.backendConf,
  query: state.query,
  google: state.google
}))(MediumRevive);