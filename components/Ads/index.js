import AdsBillboard from './Billboard';
import AdsLeaderboard from './Leaderboard';
import AdsCenter from './Center';
import AdsMedium from './Medium';
import AdsSkinner from './Skinner';
import AdsMediumRevive from './MediumRevive';
import AdsRevive from './Revive';

const initRevive = (src) => {
  const elem = document.createElement("script");
  elem.src = src;
  elem.async = true;
  elem.defer = true;
  document.body.insertBefore(elem, document.body.firstChild);
}

export {
  AdsBillboard,
  AdsLeaderboard,
  AdsCenter,
  AdsMedium,
  AdsSkinner,
  AdsMediumRevive,
  AdsRevive,
  initRevive
}