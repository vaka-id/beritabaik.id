import React from 'react';
import { connect } from 'react-redux';
import { MediaImage } from '../../Media';
import httpService from 'golibs/services/httpService';
import validation from '../validation';
import AdSense from 'react-adsense';
import Revive from '../Revive';
import './Leaderboard.scss';

class Leaderboard extends React.Component{
  state = {
    data: null,
    show:true
  }

  componentDidMount(){
    this.fetchData();
  }

  fetchData = async () => {
    let { host, baseUrl } = this.props.backendConf;
    let { position, query } = this.props;
    try{
      let ads = await validation.getAds('Leaderboard', query, this.props.backendConf, position);
      this.setState({
        data:ads,
        // show: validation.attr(ads, query.editorialSlug, query.slug, position)
      })
    }catch(error){
      console.warn(error);
    }
  }

  render(){
    const { data, show } = this.state;
    const { google, position } = this.props;
    return(
      <div className="bb-ads-leaderboard gn-primary-background">
        <div className="gn-container">
          
          { data && show ? (
            <a href={data.url}>
              <MediaImage 
                src={`${this.props.backendConf.static.host}${data.image}`}
                style={{
                  background: 'transparent'
                }}
              />
            </a>
          ) : null}

          <Revive 
            position={position} 
            type={Revive.types.LEADERBOARD}
          />
        </div>
        {/* // <div className="ads-container">
        //   <AdSense.Google
        //     client={google.adsense.client}
        //     slot={google.adsense.slotId.leaderboard.general}
        //     style={{ display: 'block' }}
        //     format='auto'
        //     responsive='true'
        //   />
        // </div> */}
      </div>
    )
  }
}

export default connect(state => ({
  backendConf:state.backendConf,
  google: state.google,
  query: state.query
}))(Leaderboard);