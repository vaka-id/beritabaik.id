import React from 'react';
import { connect } from 'react-redux';
import { MediaImage } from '../../Media';
import httpService from 'golibs/services/httpService';
import validation from '../validation';
import AdSense from 'react-adsense';
import Revive from '../Revive';

class Medium extends React.Component{
  state = {
    data: null,
    show: true
  }

  componentDidMount(){
    this.fetchData();
  }

  fetchData = async () => {
    const { host, baseUrl } = this.props.backendConf;
    const { position, index, query } = this.props;
    try{
      let ads = await validation.getAds(`Medium%20Rectangle%20${index}`, query, this.props.backendConf, position);
      this.setState({
        data:ads,
        // show: validation.attr(ads, query.editorialSlug, query.slug, position)
      })
    }catch(error){
      console.warn(error);
    }
  }

  render(){
    const { data, show } = this.state;
    const {style, className, google, position, index} = this.props;
    return(
      <div className="bb-ads-medium" style={style}>
        
        { data && show && data.image ? (
          <div className={className}>
            <a href={data.url}>
              <MediaImage 
                src={`${this.props.backendConf.static.host}${data.image}`}
                style={{
                  background: 'transparent'
                }}
              />
            </a>
          </div>
        ) : null}

        <Revive 
          position={position} 
          type={Revive.types[`MEDIUM${index}`]}
          className="gn-margin-N margin-top"
        />
        {index === 1  && String(position).toLowerCase() !== 'home' ? (
          <AdSense.Google
            client={google.adsense.client}
            slot={google.adsense.slot}
            style={{
              width: 336,
              height: 280,
              float: 'right',
              margin: '16px 0'
            }}
          />
        ) : null}
      </div>
    )
  }
}

Medium.defaultProps = {
  position: 'Home',
  style:{},
  className:'',
  index: 1
}

export default connect(state => ({
  backendConf:state.backendConf,
  query: state.query,
  google: state.google
}))(Medium);