import React from 'react';
import { connect } from 'react-redux';
import { MediaImage } from '../../Media';
import httpService from 'golibs/services/httpService';
import './Skinner.scss';
import validation from '../validation';

class Skinner extends React.Component{
  state = {
    data: null,
    show: true
  }

  componentDidMount(){
    this.fetchData();
  }

  fetchData = async () => {
    const { host, baseUrl } = this.props.backendConf;
    const { position, query } = this.props;
    try{
      let ads = await validation.getAds(`Skinner`, query, this.props.backendConf, position);
      this.setState({
        data:ads,
        // show: validation.attr(ads, query.editorialSlug, query.slug, position)
      })
    }catch(error){
      console.warn(error);
    }
  }

  render(){
    const { data, show } = this.state;
    const {style, options} = this.props;
    return(
      <div className={`bb-ads-skinner gn-layout ${ !data || !show ? 'no-show' : ''}`} style={style}>
        { data && show && data.image ? (
          <div className="skinner-item left">
            <a href={data.url}>
              <MediaImage 
                src={`${this.props.backendConf.static.host}${data.image}`}
                style={{
                  background: 'transparent'
                }}
              />
            </a>
          </div>
        ) : null}
        <div className="center-container gn-flex">{this.props.children}</div>
        { data && show && data.image ? (
          <div className="skinner-item right">
            <a href={data.url}>
              <MediaImage 
                src={`${this.props.backendConf.static.host}${data.image_2}`}
                style={{
                  background: 'transparent'
                }}
              />
            </a>
          </div>
        ) : null}
      </div>
    )
  }
}

export default connect(state => ({
  backendConf:state.backendConf,
  query: state.query
}))(Skinner);