import { isMobile, isBrowser } from 'react-device-detect';
import { httpService } from 'golibs';

const validateDevice = ads => {
  if(isMobile) return ads.is_mobile;
  else if(isBrowser) return ads.is_web;
  else return false;
}

const validation = {
  attr:(ads, channel_slug, article_slug, position) => {
    let show = false;
    if (ads && ads.id) {
      const poss = ads.position.split(',');
      const isAllArticle = poss.indexOf('Artikel') !== -1
      ads.articles_slug = ads.articles_slug.split(',');
      ads.channels_slug = ads.channels_slug.split(',');
      if (isAllArticle) {
        show =  true && validateDevice(ads);
      } else {
        const isSpecificArticle = poss.indexOf('Spesifik Artikel') !== -1
        if (isSpecificArticle) {
          if (article_slug) {
            show =  ads.articles_slug && ads.articles_slug.indexOf(article_slug) !== -1 && validateDevice(ads)
          } else {
            show = false
          }
        } else {
          show = false
        }
      }
      if (poss.indexOf('Kanal') !== -1 && channel_slug) {
        show = ads.channels_slug && ads.channels_slug.indexOf(channel_slug) !== -1 && validateDevice(ads)
      }
      if (poss.indexOf('Home') !== -1 && position === 'Home') {
        show = true && validateDevice(ads)
      }
    } else {
      show = false
    }

    return show;
  },
  getAds: (type, query, backendConf, position) => {
    let { host, baseUrl } = backendConf;
    return new Promise(async (resolve, reject) => {
      let res;
      if(position === 'Home'){
        // res = await httpService.get(`${host}${baseUrl}/ads-for-home-by-type/${type}`);
        // if(validation.attr(res.data, query.editorialSlug, query.slug, position)) resolve(res.data);
        // else resolve(null)
        resolve(null)
      } else if(position === 'Artikel'){
        res = await httpService.get(`${host}${baseUrl}/ads-for-specific-article-by-slug-and-type/${query.slug}/${type}`);
        if(validation.attr(res.data, query.editorialSlug, query.slug, position)) resolve(res.data);
        else resolve(null)
      } else if(position === 'Kanal'){
        // res = await httpService.get(`${host}${baseUrl}/ads-for-channel-by-slug-and-type/${query.editorialSlug}/${type}`);
        // if(validation.attr(res.data, query.editorialSlug, query.slug, position)) resolve(res.data);
        // else resolve(null);
        resolve(null)
      } else resolve(null);

    })
  } 
}


export default validation;