import React from 'react';
import { connect } from 'react-redux';
import { MediaImage } from '../../Media';
import httpService from 'golibs/services/httpService';
import validation from '../validation';
import AdSense from 'react-adsense';
import Revive from '../Revive';
import './Center.scss';

class Center extends React.Component{
  state = {
    data: null,
    show: true
  }

  componentDidMount(){
    this.fetchData();
  }

  fetchData = async () => {
    const { host, baseUrl } = this.props.backendConf;
    const { position, index, query } = this.props;
    try{
      let ads = await validation.getAds(`Center%20Ads%20${index}`, query, this.props.backendConf, position);
      this.setState({
        data:ads,
        // show: validation.attr(ads, query.editorialSlug, query.slug, position)
      })
    }catch(error){
      console.warn(error);
    }
  }

  render(){
    const { data, show } = this.state;
    const {style, className, google, index, position} = this.props;
    return(
      <div className={`bb-ads-center`} style={style}>
        
        { data && show && data.image ? (
          <div className={className}>
            <a href={data.url}>
              <MediaImage 
                src={`${this.props.backendConf.static.host}${data.image}`}
              />
            </a>
          </div>
        ) : null}

        { index === 1 || index === 2 ? (
        <Revive 
          position={position} 
          type={Revive.types[`CENTER${index}`]}
          className="gn-margin-N margin-top"
        />
        ) : null}
        { index === 1 ? (
          <div className="ads-container ">
            <AdSense.Google
              client={google.adsense.client}
              slot={google.adsense.slot}
              style={{ display: 'block' }}
              format='auto'
              responsive='true'
            />
          </div>
         ) : null}
         { index === 2 || index === 3 || index === 4 ? (
          <div className="ads-container ">
            <AdSense.Google
              client={google.adsense.client}
              slot={google.adsense.slot}
              style={{ display: 'block' }}
              format='auto'
            />
          </div>
         ) : null}
      </div>
    )
  }
}

Center.defaultProps = {
  position: 'Home',
  style:{
    textAlign:'center'
  },
  className:'',
  index: 1
}

export default connect(state => ({
  backendConf:state.backendConf,
  google:state.google,
  query: state.query
}))(Center);