import React from 'react';
import { Card, Input, Button, Checkbox, message } from 'antd';
import { connect } from 'react-redux';
import config from '../../server/config/environment';
import './LoginPanel.scss';
import httpService from 'golibs/services/httpService';
import Router from 'next/router';
import Oauth2 from '../Oauth2';

const defaultState = () => ({
  onProgress: false,
  isMounted:false,
  formData:{
    username:'',
    password:''
  }
})

class LoginPanel extends React.Component{
  constructor(){
    super();
    this.state = defaultState();
  }

  componentDidMount(){
    this.setState({
      isMounted:false
    })
  }

  formHandleChange = (key, value) => {
    let { formData } = this.state;
    formData[key] = value;
    this.setState({formData});
  }

  onFormSubmit = async e => {
    e.preventDefault();
    e.stopPropagation();
    
    const defState = defaultState();
    const { onProgress } = defState; 
    this.setState({
      onProgress
    })
    
    const { host, baseUrl } = config.backend;
    let { formData } = this.state;
    this.setState({onProgress:true})
    try{
      await httpService.post(`/auth/login`, formData);
      let targetUri = this.props.query.reuri;
      window.open( targetUri ? window.atob(targetUri) : '/', '_self');  
    }catch(error){
      message.error('Gagal Login');
      formData.password = '';
      this.setState({
        formData, 
        onProgress:false
      })
    }
  }

  render(){
    const { formData, onProgress } = this.state;

    return(
      <div className="bb-login-panel">
        <Card>
          <div className="brand">
            <a href="/">
              <img src="/static/images/logo-berita-baik-pm.png" alt="logo berita baik"/>
            </a>
          </div>
          
          <form onSubmit={this.onFormSubmit}>
             <Input
                placeholder="Username atau Alamat Surel"
                value={formData.username}
                onChange={ e => this.formHandleChange('username', e.target.value)}
                disabled={onProgress}
                prefix={
                  <i className="fas fa-user icon"/>
                }
              />
              <Input
                placeholder="Password"
                value={formData.password}
                onChange={ e => this.formHandleChange('password', e.target.value)}
                disabled={onProgress}
                prefix={
                  <i className="fas fa-lock icon"/>
                }
                type="password"
                required
              />
              <div className="action gn-margin-S margin-top gn-layout align-center justify-between">
                <div>
                  <a>Lupa Sandi?</a>
                  <Checkbox>Ingat Saya</Checkbox>
                </div>
                <Button className="font-primary" type="primary" htmlType="submit" disabled={onProgress}>
                  <span className="font-primary">Masuk</span>
                </Button>
            </div>
          </form>
          <Oauth2/>
          <div className="misc">
            <span>Belum Mendaftar ? <a href="/sign-up">Daftar Sekarang</a></span>
          </div>
        </Card>
      </div>
    )
  }
}

export default connect( state => ({
  query: state.query
}))(LoginPanel);