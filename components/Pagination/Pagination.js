import React from 'react';
import queryString from 'query-string';
import _ from 'lodash';
import './Pagination.scss';

let med = 2

const generateHref = (page, hrefOption) => {
    return `${hrefOption.url}?${queryString.stringify(
        (() => {
            let q = _.clone(hrefOption.query)
            q.page = page;
            return q;
        })()
    )}`
}

const generatePage = (page, pageSize, total, hrefOption) => {
    let pages = [];    
    page = Number(page);
    let min = page - med    
    if(min <= 0) {
        med += Math.abs(min - 1)        
        min = 1;
    }

    if(min > 1) pages.push({
        value:'<',
        href: generateHref(min - 1, hrefOption)
    })

    let max = page + med
    let tot = total/pageSize;

    max = max > tot ? tot : max

    for(let p = min ; p <= max ; p++){
        pages.push({
            value:p,
            href:generateHref(p, hrefOption)
        })
    }

    if(tot > max) pages.push({
        value:'>',
        href: generateHref(max+1, hrefOption)
    })
    
    return pages;
} 

const getMax = (page, pageSize, total) => {
    return total < pageSize ? total : (page * pageSize);
}

const Pagination = ({page, pageSize, total, className, hrefOption}) => (
    <div className={`bb-pagination ${className?className:''}`}>
        <div className="paging">
            {generatePage(page, pageSize, total, hrefOption).map((d, i) => (
                <li key={i} className={`${d.value == page ? 'active':''}`}>
                    <a href={d.href}>
                        {d.value}
                    </a>
                </li>
            ))}
        </div>
    </div>
)

export default Pagination;