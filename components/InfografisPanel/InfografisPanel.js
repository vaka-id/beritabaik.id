import React from 'react';
import ContentPanel from '../ContentPanel';
import httpService from 'golibs/services/httpService';
import config from 'server/config/environment';
import { MediaImage } from '../Media'
import qs from 'query-string';
import { ArticleLink } from '../Article';

class InfografisPanel extends React.Component{
  state = {data: null};

  componentDidMount(){
    this.fetchData();
  }

  fetchData = async () => {
    const { host, baseUrl } = config.backend;
    try{
      const res = await httpService.get(`${host}${baseUrl}/articles-editorial/infografis?${qs.stringify({
        editorialSlug:'infografis',
        page: 1,
        per_page: 1
      })}`)
      this.setState({data:res.data.data});
    }catch(error){
      console.warn(error);
    }
  }

  render(){
    const { data } = this.state;
    return(
      <ContentPanel
        title="Infografis"
        moreLink="/kanal?editorialSlug=infografis&page=1"
        withContentBorder
      >
        {data ? (
          <ArticleLink article={data[0]}>
            <MediaImage
              src={`${config.backend.static.host}${data[0].thumb_image}`}
            />
          </ArticleLink>
        ) : null }
      </ContentPanel>
    )
  }
}

export default InfografisPanel;