import React from 'react';
import { connect } from 'react-redux';
import { Button, message } from 'antd';
import { Facebook, Google } from 'react-oauth2';
import './Oauth2.scss';
import { httpService } from 'golibs';

class Oauth2Item extends React.Component{
  oauthCallback = async ( oauth, res, err ) => {
    if(err){
      message.error('Gagal login');
    } else {
      try{
        await httpService.post(`/auth/login?oauth=${oauth}`, res.profile);
        let targetUri = this.props.query.reuri;
        window.open( targetUri ? window.atob(targetUri) : '/', '_self');  
      }catch(error){
        message.error('Gagal Login');
        formData.password = '';
        this.setState({
          formData, 
          onProgress:false
        })
      }
    }
    // if (!err) {
    //   this.setState({ data: res.profile })
    // } else {
    //   this.setState({ data: 'something happen wrong' })
    // }
  }

  render(){
    const { webAddress, facebook, google} = this.props;
    const WithFacebook = (
      <Facebook 
        url={webAddress}
        clientId={facebook.appId}
        redirectUri={webAddress}
        scope={'email,user_location'}
        callback={(res, err) => this.oauthCallback('facebook', res, err)}
        style={{
          background: '#4267b2'
        }}
      >
        <i className="fab fa-facebook-f"/>
      </Facebook> 
    )

    const WithGoogle = (
      <Google
        url={webAddress}
        clientId={google.clientId}
        clientSecret={google.clientSecret}
        redirectUri={webAddress}
        scope={['https://www.googleapis.com/auth/userinfo.email']}
        width={300}
        height={300}
        callback={(res, err) => this.oauthCallback('google', res, err)}
        style={{ background: '#dd4b39' }}
      >
        <i className="fab fa-google"/>
      </Google>
    )

    return (
      <div className="bb-oauth2">
        {WithFacebook}
        {WithGoogle}
      </div>
    )
  }
}

export default connect( state => ({
  query: state.query,
  webAddress: state.webAddress,
  facebook: state.facebook,
  google: state.google
}))(Oauth2Item);

// const test = ({withAddress, appId}) => (
//   <Facebook 
//     url={webAddress}
//     clientId={appId}
//     redirectUri={webAddress}
//     scope={'email,user_location'}
//     callback={(res, err) => {}}
//     style={{
//       background: '#4267b2'
//     }}
//   >
//     <i className="fab fa-facebook-f"/>
//   </Facebook> 
// )

// export default test;