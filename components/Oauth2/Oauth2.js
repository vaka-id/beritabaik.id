import React from 'react';
import { connect } from 'react-redux';
import './Oauth2.scss';

class Oauth2 extends React.Component{
  constructor(){
    super();
    this.state = {
      isMounted:false
    }
  }

  componentDidMount(){
    this.setState({isMounted:true})
  }

  render(){
    return (
      <div className="bb-oauth2">
        {this.state.isMounted && React.createElement(
          require('./OAuth2Item').default
        )}
      </div>
    )
  }
}

export default Oauth2;