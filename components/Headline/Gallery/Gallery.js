import React from 'react';
import httpService from 'golibs/services/httpService';
import Swiper from 'react-id-swiper';
import { Button } from 'antd';
import config from 'server/config/environment';
import qs from 'query-string';
import { MediaImage } from '../../Media'
import { ArticleAuthor, ArticleLink } from '../../Article';
import Separator from '../../Separator';

import './Gallery.scss';

const slideDuration = 6000;
const slideSpeed = 1000;

class HeadlineGallery extends React.Component{
  constructor(){
    super();
    this.swiper = null;
    this.galleryRef = this.galleryRef.bind(this);
    this.thumbRef = this.thumbRef.bind(this);
    this.state = {
      swiperIndex:0,
      gallerySwipper: null,
      thumbnailSwipper: null,
      data:[],
      setToMiddle:true
    };
  }

  componentDidMount(){
    if(!this.props.data) this.fetchData();
  }

  componentWillUpdate(nextProps, nextState) {
    if (nextState.gallerySwiper && nextState.thumbnailSwiper) {
      const { gallerySwiper, thumbnailSwiper } = nextState

      if(gallerySwiper && thumbnailSwiper){
        gallerySwiper.controller.control = thumbnailSwiper;
        thumbnailSwiper.controller.control = gallerySwiper;
        if(this.state.setToMiddle){
          this.setToMiddle();
        }
      }
    }
  }

  setToMiddle(){
    let { gallerySwiper, thumbnailSwiper} = this.state;
    const run = (delay) => {
      setTimeout(() => {
        if(gallerySwiper) gallerySwiper.slideNext(0);
      }, delay)
    }

    run(0); run(0);
    this.setState({setToMiddle:false})
  }

  fetchData = async () => {
    try{
      let res = await httpService.get(`${config.backend.host}${config.backend.baseUrl}/articles-editorial/gallery-foto?${qs.stringify({
        editorialSlug:'gallery-foto',
        page:1,
        per_page: 5
      })}`)
      this.setState({data:res.data.data})
    }catch(error){
      console.log(error);
    }
  }

  galleryRef(ref) {
    if (ref) this.setState({ gallerySwiper: ref.swiper })
  }

  thumbRef(ref) {
    if (ref) this.setState({ thumbnailSwiper: ref.swiper })
  }

  goNext = () => {
    const {gallerySwipper} = this.state;
    if (gallerySwipper) {
      gallerySwipper.slideNext(slideSpeed)
      let { swiperIndex, data } = this.state;
      if(swiperIndex < data.length - 1){
        swiperIndex++;
        this.setState({swiperIndex});
      }
    }
  }

  goPrev = () => {
    const {gallerySwipper} = this.state;
    if (gallerySwipper) {
      gallerySwipper.slidePrev(slideSpeed);
      let { swiperIndex } = this.state;
      if(swiperIndex > 0){
        swiperIndex--;
        this.setState({swiperIndex});
      }
    }
  }

  render(){
    let data = this.props.data || this.state.data;
    const { swiperIndex } = this.state;
    const params = {
      slidesPerView: 'auto',
      spaceBetween: 0,
      slidesPerGroup: 1,
      loop: false,
      grabCursor: false,
      lazy: true,
      loopFillGroupWithBlank: true,
      activeSlideKey: 3
    };

    const thumbnailSwiperParams = {
      centeredSlides: true,
      spaceBetween: 16,
      slidesPerView: 5,
      touchRatio: 0.2,
      slideToClickedSlide: true,
      activeSlideKey: 3
    };

    return(
      <div className="bb-headline-gallery">
        <div className="gn-container">
          {data.length > 0 ? (
            <div>
              <Swiper {...params} ref={this.galleryRef}>
                  {data.map((d, i) => (
                      <div className="swiper-item" key={`swiper-item-gallery-${i}`}>
                        <ArticleLink article={d}>
                          <MediaImage 
                            src={`${config.backend.static.host}${d.main_image}`} alt={d.title}
                            label="Galeri Foto"
                            labelSize="large"
                          />
                        </ArticleLink>
                        <div className="body">
                          <ArticleLink article={d}>
                            <h1>{d.title}</h1>
                          </ArticleLink>
                          <p>{d.teaser}</p>
                          <ArticleAuthor
                            author={d.reporter_name} 
                            publishedAt={d.publish_date}
                          />
                        </div>
                      </div>
                  ))}
                </Swiper>
                <div className="thumb-container">
                  <Swiper {...thumbnailSwiperParams} ref={this.thumbRef}>
                    {data.map((d, i) => (
                      <div className="swiper-thumb-item" key={`swiper-thumb-item-${i}`}>
                        <MediaImage key={`swiper-item-gallery-thumb-${i}`}
                          src={`${config.backend.static.host}${d.main_image}`} alt={d.title}
                        />
                      </div>
                    ))}
                  </Swiper>
                </div>
                <Separator/>
              </div>
          ) : null}
        </div>
      </div>
    )
  }
}

HeadlineGallery.props = {
  data:null
}

export default HeadlineGallery;