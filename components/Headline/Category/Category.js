import React from 'react';
import dynamic from 'next/dynamic';
import Swiper from 'react-id-swiper';
import { Button } from 'antd';
import './Category.scss';
import httpService from 'golibs/services/httpService';
import queryString from 'query-string';
import config from '../../../server/config/environment';
import { ArticleLink } from '../../Article';
import { MediaImage } from '../../Media';

// const Swiper = dynamic(import('react-id-swiper'));

class HeadlineCategory extends React.Component{
  constructor(){
    super();
    this.swiper = null;
    this.state = {
      swiperIndex: 0,
      isMobile: false,
      data:[]
    }
  }

  componentDidMount(){
    if(window.outerWidth < 768) this.setState({isMobile:true});
    if(!this.props.data) this.fetchData();
  }

  fetchData = async () => {
    const { host, baseUrl } = config.backend;
    try{
      let res = await httpService.get(`${host}${baseUrl}/topslide-articles?${queryString.stringify({
        page:1,
        per_page: 9
      })}`)
      this.setState({data:res.data.data})
    } catch(error){

    }
  }

  goNext = () => {
    if (this.swiper) {
      this.swiper.slideNext()
      let { swiperIndex, data } = this.state;
      if(swiperIndex < data.length - 3){
        swiperIndex++;
        this.setState({swiperIndex});
      }
    }
  }

  goPrev = () => {
    if (this.swiper) {
      this.swiper.slidePrev();
      let { swiperIndex } = this.state;
      if(swiperIndex > 0){
        swiperIndex--;
        this.setState({swiperIndex});
      }
    }
  }

  render(){
    const data = this.props.data || this.state.data;
    const { swiperIndex, isMobile } = this.state;
    const params = {
      slidesPerView: isMobile ? 2 : 3,
      centeredSlides: false,
      spaceBetween: isMobile ? 16 : 48,
      slidesPerGroup: 1,
      loop: false,
      grabCursor: true,
      lazy: false,
      loopFillGroupWithBlank: true,
      // pagination: {
      //   el: '.swiper-pagination',
      //   clickable: true,
      // }
    };

    return(
      <div className="bb-headline-category gn-grey-background">
          {data.length > 0 ? (
            <div className="gn-container">
              <div className="flex-none control left gn-margin-M margin-right">
                <Button 
                  onClick={this.goPrev}
                  type="primary"
                  shape="circle"  
                  className="gn-parimary-background"
                  disabled={swiperIndex === 0}
                >
                  <i className="fas fa-arrow-left"/>
                </Button>
              </div>
              <div className="swiper-block">
                <div>
                  <Swiper {...params} ref={node => {if(node) this.swiper = node.swiper }}>
                    {data.map((d, i) => (
                      <div className="swiper-item" key={`swiper-item-category-${i}`}>
                        <ArticleLink article={d}>
                          <MediaImage src={`${config.backend.static.host}${d.thumb_image}`} alt={d.title}/>
                          <div className="label gn-primary-background font-secondary">
                            {d.editorial.name}
                          </div>
                        </ArticleLink>
                      </div>
                    ))}
                  </Swiper>
                </div>
              </div>
              <div className="flex-none control right gn-margin-M margin-left">
                <Button 
                  onClick={this.goNext}
                  type="primary"
                  shape="circle"
                  disabled={swiperIndex === data.length - 3}
                >
                  <i className="fas fa-arrow-right"/>
                </Button>
              </div>
            </div>
          ) : <div className="flex"/> }
      </div>
    )
  }
}

HeadlineCategory.defaultProps = {
  data:null
}

export default HeadlineCategory;