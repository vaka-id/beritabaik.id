import React from 'react';
import httpService from 'golibs/services/httpService';
import config from 'server/config/environment';
import { ArticleAuthor } from '../../Article';
import qs from 'query-string';
import YouTube from 'react-youtube';
import Separator from '../../Separator';
import './Video.scss';

const getVideoId = (source) => {
  return qs.parseUrl(source).query.video_id;
}

class Video extends React.Component{
  state = {data: null}
  componentDidMount(){
    this.fetchData();
  }

  fetchData = async () => {
    const { host, baseUrl } = config.backend;
    try{
      let res = await httpService.get(`${host}${baseUrl}/articles-editorial/latest/video?${qs.stringify({
        editorialSlug:'video',
        type:'video'
      })}`)
      this.setState({data:res.data})
    }catch(error){
      console.warn(error)
    }
  }

  render(){
    const { data } = this.state;

    return(
      <div className="bb-headline-video gn-margin-N margin-bottom">
        <div className="gn-container">
          { data ? (
            <div className="video-item">
              <YouTube
                videoId={getVideoId(data.sources_path)}
                opts={{
                  height: '100%',
                  width: '100%',
                  playerVars: { // https://developers.google.com/youtube/player_parameters
                    autoplay: 0
                  }
                }}
              />
              <div className="body">
                <div className="label gn-primary-background gn-margin-N margin-bottom">
                  Video
                </div>
                <h1 className="font-secondary">{data.title}</h1>
                <ArticleAuthor
                  author={data.reporter_name} 
                  publishedAt={data.publish_date}
                />
              </div>
              <Separator/>
            </div>
          ) : null}
        </div>
      </div>
    )
  }
}

export default Video;