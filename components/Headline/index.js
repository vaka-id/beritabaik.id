import HeadlineCategory from './Category';
import HeadlineArticle from './Article';
import HeadlineEvent from './Event';
import HeadlineGallery from './Gallery';
import HeadlineVideo from './Video';

export {
  HeadlineArticle,
  HeadlineCategory,
  HeadlineEvent,
  HeadlineGallery,
  HeadlineVideo
}