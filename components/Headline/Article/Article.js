import React from 'react';
import httpService from 'golibs/services/httpService';
import Swiper from 'react-id-swiper';
import { Button, Row, Col } from 'antd';
import config from '../../../server/config/environment';
import queryString from 'query-string';
import moment from 'moment';
import { ArticleAuthor, ArticleActions } from '../../Article';

import './Article.scss';
import { MediaImage } from '../../Media';
import { ArticleLink } from '../../Article';

const slideDuration = 6000;
const slideSpeed = 1000;

class HeadlineArticle extends React.Component{
  constructor(){
    super();
    this.swiper = null;
    this.state = {
      swiperIndex:0,
      data:[],
      showControl: false
    };
  }

  componentDidMount(){
    if(!this.props.data) this.fetchData();
    this.fireEventOnHover()
  }

  fetchData = async () => {
    try{
      let res = await httpService.get(`${config.backend.host}${config.backend.baseUrl}/headline-articles?${queryString.stringify({
        page:1,
        per_page: 9
      })}`)
      this.setState({data:res.data.data}, () => {
        setTimeout(this.fireEventOnHover, 500)
      })
    }catch(error){
      console.log(error);
    }
  }

  fireEventOnHover = () => {
    let btns = document.getElementsByClassName('swiper-pagination-bullet');
    for(let i = 0 ; i < btns.length ; i++){
      let btn = btns[i];
      btn.onmouseover = () => {
        btn.click();
      }
    }
  }

  goNext = () => {
    if (this.swiper) {
      this.swiper.slideNext(slideSpeed)
      let { swiperIndex, data } = this.state;
      if(swiperIndex < data.length - 1){
        swiperIndex++;
        this.setState({swiperIndex});
      }
    }
  }

  goPrev = () => {
    if (this.swiper) {
      this.swiper.slidePrev(slideSpeed);
      let { swiperIndex } = this.state;
      if(swiperIndex > 0){
        swiperIndex--;
        this.setState({swiperIndex});
      }
    }
  }

  handleMouseOver = () => {this.setState({showControl: true})
  }

  render(){
    let data = this.props.data || this.state.data;
    const { swiperIndex, showControl } = this.state;
    const { className='' } = this.props;
    const params = {
      slidesPerView: 'auto',
      centeredSlides: true,
      spaceBetween: 48,
      slidesPerGroup: 1,
      loop: data && data.length > 1 ? true : false,
      grabCursor: data && data.length > 1 ? true : false,
      lazy: false,
      loopFillGroupWithBlank: true,
      rebuildOnUpdate: false,
      autoplay: {
        delay: slideDuration,
        disableOnInteraction: false
      },
      pagination: data && data.length > 1 ? {
        el: '.swiper-pagination',
        type: 'bullets',
        clickable: true
      } : {}
    };

    return(
      <div className={`bb-headline-article gn-grey-background gn-padding-M padding-bottom ${className}`}>
       
          {data.length > 0 ? (
             <div 
              className={`gn-container gn-layout align-center`}
              onMouseOver={() => this.setState({showControl: true})}
              onMouseLeave={() => this.setState({showControl: false})}
            >
              <div className="flex-none control left gn-margin-M margin-right">
                <Button 
                  onClick={this.goPrev}
                  type="primary"
                  shape="circle"
                  style={{display: data.length > 1 ? 'inline' : 'none'}}
                  className={this.state.showControl ? 'show' : 'hide'}
                >
                  <i className="fas fa-chevron-left"/>
                </Button>
              </div>
              <Swiper {...params} ref={node => {if(node) this.swiper = node.swiper }}>
                {data.map((d, i) => (
                    <div 
                      className="swiper-item" 
                      key={`swiper-item-article-${i}`} 
                    >
                      <Row type="flex" className="content">
                        <Col md={14}>
                          <ArticleLink article={d}>
                            <div className="image-container">
                              <MediaImage src={`${config.backend.static.host}${d.thumb_image}`} alt={d.title}/>
                            </div>
                          </ArticleLink>
                        </Col>
                        <Col md={10}>
                          <div className="body">
                            <ArticleLink article={d}>
                              <h1>{d.title}</h1>
                            </ArticleLink>
                            <p>{d.teaser}</p>
                            <ArticleAuthor 
                              author={d.reporter_name} 
                              publishedAt={d.publish_date}
                            />
                            <ArticleActions article={d}/>
                          </div>
                        </Col>
                      </Row>
                      <div className="label gn-primary-background font-secondary">
                        {d.editorial.name}
                      </div>
                    </div>
                ))}
              </Swiper>
              <div className="flex-none control right gn-margin-M margin-left">
                <Button 
                  onClick={this.goNext}
                  type="primary"
                  shape="circle"
                  style={{display: data.length > 1 ? 'inline' : 'none'}}
                  className={this.state.showControl ? 'show' : 'hide'}
                >
                  <i className="fas fa-chevron-right"/>
                </Button>
              </div>
            </div>
          ) : <div className="flex"/>}
      </div>
    )
  }
}

HeadlineArticle.props = {
  data:null
}

export default HeadlineArticle;