import React from 'react';
import { Col, Row, Button } from 'antd';
import Separator from '../../Separator';
import './Event.scss';
import httpService from 'golibs/services/httpService';
import config from '../../../server/config/environment';
import { connect } from 'react-redux';
import moment from 'moment';

class Event extends React.Component{
  state = {
    data:[],
    noCurrentMonthData: false
  }

  componentDidMount(){
    this.setState({date:this.props.today});
    this.fetchData(true);
  }

  fetchData = async (firstLoad) => {
    const { host, baseUrl } = config.backend;
    const { date } = this.state;
    try{
      let res = await httpService.get(`${host}${baseUrl}/events-by-period/${moment(date).format('YYYY')}${moment(date).format('MM')}`)
      this.setState({
        data:res.data,
        noCurrentMonthData: firstLoad && res.data.length === 0 ? true : false
      });
    }catch(error){
      console.warn(error)
    }
  }

  newMonth = isPrevious => {
    let { date } = this.state;
    date = moment(date).add( isPrevious ? -1 : 1, 'month');
    this.setState({date})
    setTimeout(this.fetchData);
  }

  render(){
    const { date, data } = this.state;
    const { today } = this.props;
    const EventContent = (d) => (
      <div className="gn-layout align-center">
        <div className="date gn-layout align-center justify-center">
        { d ? moment(d.date).format('DD') : '-'}
        </div>
        { d ? (
          <a href={`/event-details?id=${d.id}`}>
            <div className="description">{d.title}</div>
          </a>
        ) : null}
      </div>
    )

    return(
      <div 
        className="bb-headline-event"
        style={{
          display: this.state.noCurrentMonthData ? 'none' : 'inline'
        }}
        >
        <div className="gn-container gn-padding-N padding-top">
          <div className="header">
            <div className="gn-layout align-center">
              <div>Acara Bulan Ini</div>
              <Button
                type="circle"
                onClick={() => this.newMonth(true)}
                // disabled={moment(today).format('lll') === moment(date).format('lll')}
              >
                <i className="fas fa-chevron-left"/>
              </Button>
              <Button
                type="circle"
                onClick={() => this.newMonth()}
              >
                <i className="fas fa-chevron-right"/>
              </Button>
            </div>
            <div className="red-line gn-secondary-background"/>
          </div>
          <div>
            <Row type="flex" align="center" className="gn-layout column-sm">
              <Col span={6}>
                <div className="event-item font-secondary">
                  <a href="/event">{moment(date).format('MMMM')}</a>
                </div>
              </Col>
              <Col span={6}>
                <div className="event-item">
                  {EventContent(data[0])}
                </div>
              </Col>
              <Col span={6}>
                <div className="event-item">
                  {EventContent(data[1])}
                </div>
              </Col>
              <Col span={6}>
                <div className="event-item">
                  {EventContent(data[2])}
                </div>
              </Col>
            </Row>
          </div>
          <Separator/>
        </div>
      </div>
    )
  }
}

export default connect( state => ({
  today: state.today
}))(Event);