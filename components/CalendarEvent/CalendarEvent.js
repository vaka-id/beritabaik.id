import React from 'react';
import Calendar from './__components/Calendar';
import Headline from './__components/Headline';
import EventDetails from './__components/EventDetails';
import { Button } from 'antd';
import './CalendarEvent.scss';

class CalendarEvent extends React.Component{
  render(){
    const {today, premiumEvents} = this.props;
    return(
      <div className="bb-calendar-event gn-grey-background gn-padding-M padding-bottom padding-top">
        <div className="gn-container">
          <Headline data={premiumEvents}/>
          <Calendar today={today}/>
        </div>
      </div>
    )
  }
}

CalendarEvent.Details = ({premiumEvents, event}) => (
  <div className="bb-calendar-event gn-grey-background gn-padding-M padding-bottom padding-top">
    <div className="gn-container">
      <Headline data={premiumEvents}/>
      <div style={{background:'white'}} className="events gn-margin-L margin-top gn-padding-L padding-all">
        <div className="gn-primary-color font-secondary-medium title-bordered gn-margin-N margin-bottom">Acara</div>
        <EventDetails event={event}/>
        <a href="/event"><Button className="gn-margin-N margin-top">Acara Lainnya</Button></a>
      </div>
    </div>
  </div>
);

export default CalendarEvent;