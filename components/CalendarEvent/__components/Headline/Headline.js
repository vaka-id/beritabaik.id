import React from 'react';
import { connect } from 'react-redux';
import Swiper from 'react-id-swiper';
import { MediaImage } from '../../../Media';
import moment from 'moment';
import './Headline.scss'

const Headline = ({data, backendConf}) => {
  return (
    <div className="bb-headline-events">
      <Swiper
        params={{
          slidesPerView: 'auto',
          centeredSlides: true,
          spaceBetween: 48,
          slidesPerGroup: 1,
          loop: data && data.length > 1 ? true : false,
          grabCursor: data && data.length > 1 ? true : false,
          lazy: true,
          loopFillGroupWithBlank: true,
          rebuildOnUpdate: false,
          autoplay: {
            delay: 6000,
            disableOnInteraction: false
          },
          pagination: {
            el: '.swiper-pagination',
            type: 'bullets',
            clickable: true
          }
        }}
      >
        {data.map((event, i) => (
          <div className="event gn-layout column-sm" key={i}>
            <MediaImage
              src={`${backendConf.static.host}${event.image}`}
              alt={event.title}
            />
            <div className="body gn-padding-N padding-all gn-flex font-secondary">
              <h2 className="gn-secondary-color">{event.title}</h2>
              <p>{event.description}</p>
              <ul className="info">
                <li className="font-secondary">
                  <i className="far fa-clock"/>
                  {`${moment(event.date).format('LL')} - ${moment(event.end_date).format('LL')}`}
                </li>
                <li className="font-secondary">
                  <i className="fas fa-map-marker-alt"/>
                  {`${event.place}`} <span className="small">{event.address}</span>
                </li>
              </ul>
            </div>
          </div>
        ))}
      </Swiper>
    </div>
  )
}

export default connect( state => ({
  backendConf: state.backendConf
}))(Headline);