import React from 'react';
import { connect } from 'react-redux';
import { MediaImage } from '../../../Media';
import moment from 'moment';
import './EventDetails.scss';

class EventDetails extends React.Component{
  render(){
    const { event, i, backendConf } = this.props;
    return(
      <div className="event-details gn-layout column-sm gn-padding-N padding-all">
        <MediaImage
          src={`${backendConf.static.host}${event.image}`}
          alt={event.title}
        />
        <div className="body gn-margin-N margin-left">
          <h3 className="gn-secondary-color font-secondary">Details</h3>
          <p>{event.description}</p>
          <ul className="info">
            <li className="font-secondary">
              <i className="far fa-clock"/>
              {`${moment(event.date).format('LL')} - ${moment(event.end_date).format('LL')}`}
            </li>
            <li className="font-secondary">
              <i className="fas fa-map-marker-alt"/>
              {`${event.place}`} <span className="small">{event.address}</span>
            </li>
            { event.ticketing_url ? (
            <li className="font-secondary">
              <i className="fas fa-tag"/>             
              {`Ticketing`} <a target="_blank" href={event.ticketing_url}>{`[Registration Here]`} 
              </a>
            </li>
            ) : '' }
          </ul>
        </div>
      </div>
    )
  }
}

export default connect(state => ({
  backendConf:state.backendConf
}))(EventDetails);