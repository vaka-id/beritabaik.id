import React from 'react';
import moment from 'moment';
import { connect } from 'react-redux';
import { Button, message, Collapse } from 'antd';
import _ from 'lodash';
import { MediaImage } from '../../../Media';
import './Calendar.scss';
import httpService from 'golibs/services/httpService';
import EventDetails from '../EventDetails';

const Panel = Collapse.Panel;
const arrDays = ['Sun', 'Mon', 'Thu', 'Wed', 'Tue', 'Fri', 'Sat'];

class Calendar extends React.Component{
  constructor(){
    super();
    this.state = {
      selectedDate:null,
      isLoading:false,
      dateTable:[],
      dateGroups:[],
      month:null,
      year:null,
      events:[],
      eventKeys:[],
      filteredEvents:[]
    }
  }
  componentDidMount(){
    const today = moment(this.props.today);
    this.fetchEvent(today.month() + 1, today.year());
  }

  fetchEvent = async (month, year) => {
    this.setState({isLoading:true});
    const { host, baseUrl } = this.props.backendConf;
    const date = moment(`${month}/${year}`, 'MM/YYYY').format('YYYYMM');
    try{
      let res = await httpService.get(
        `${host}${baseUrl}/events-by-period/${date}`
      )
      let events = res.data.map( d => {
        d.dateString = moment(d.date).format('DD/MM/YYYY');
        return d;
      })

      let filteredEvents = _.cloneDeep(events);
      this.setState({events, filteredEvents, isLoading:false}, () => {
        this.generateView(month, year);
      });
    }catch(error){
      this.setState({isLoading:false});
      this.generateView(month, year);
      message.error('Gagal memuat kalender')
    }
  }

  nextMonth = (isPrevious) => {
    const { month, year } = this.state;
    let date = moment(`1/${month}/${year}`,'DD/MM/YYYY');
    date = date.add( isPrevious ? -1 : 1 , 'months');
    this.fetchEvent(date.month() + 1, date.year());
  }

  generateView = async (month, year) => {
    let _format = 'DD/MM/YYYY'
    let today = moment(this.props.today);
    const firstDate = moment(`1/${month}/${year}`, _format);
    const totalDayInMonth = firstDate.daysInMonth();
    const lastDate = moment(`${totalDayInMonth}/${month}/${year}`, _format);
    const { events } = this.state;
    let totalWeek = Math.ceil((firstDate.day() + lastDate.date())/7);
    let dateGroups = [];
    for( let i = 0 ; i < totalWeek ; i++){
      dateGroups.push(arrDays.map(() => (null)))
    }

    let datesOfMonth = [];
    for( let i = 1 ; i <= totalDayInMonth ; i++){
      const date = moment(`${i}/${month}/${year}`, _format);
      const event = _.find(events, {
        dateString: date.format(_format)
      })
      const dateObject = {
        date, event,
        isToday: date.isSame(moment(`${today.date()}/${today.month()+1}/${today.year()}`, _format)),
        day: date.day(),
        inWeek: Math.ceil((firstDate.day() + date.date())/7)
      }
      datesOfMonth.push(dateObject)
      dateGroups[dateObject.inWeek - 1][dateObject.day] = dateObject;
    }

    this.setState({
      dateTable: datesOfMonth, dateGroups,
      month, year, eventKeys:[]
    });
  }

  selectDate = (date) => {
    let { selectedDate, filteredEvents, events } = this.state;
    selectedDate = selectedDate && selectedDate.isSame(date) ? null : date;
    if(selectedDate){
      filteredEvents = events.filter((d) => {
        return d.dateString === selectedDate.format('DD/MM/YYYY');
      })
    } else {
      filteredEvents = _.cloneDeep(events)
    }
    this.setState({selectedDate, filteredEvents});
  }

  render(){
    const { dateTable, dateGroups, month, year, selectedDate } = this.state; 
    const { backendConf } = this.props;
    const hasEvent = (dg) => {
      return dg ? (dg.event ? true : false) : false;
    }
    const isSelectedDate = (dg) => {
      return dg ? (dg.date ? selectedDate && selectedDate.isSame(dg.date) : false) : false;
    }
    const Control = (
      <div className="control gn-margin-M margin-bottom">
        <div className="gn-layout align-center justify-end">
          <Button 
            shape="circle" 
            onClick={() => this.nextMonth(true)}
            disabled={this.state.isLoading}
          >
            <i className="fas fa-chevron-left"/>
          </Button>
          <div className="current-date font-secondary gn-padding-L padding-left padding-right">
            {`${month ? month : ' - '}/${year ? year : ' - '}`}
          </div>
          <Button 
            shape="circle" 
            onClick={() => this.nextMonth()}
            disabled={this.state.isLoading}
          >
            <i className="fas fa-chevron-right"/>
          </Button>
        </div>
      </div>
    )

    const DateTable = (
      <table className="calendar-table">
        <thead>
          <tr>
            {arrDays.map( day => (
              <th key={day}>{day}</th>
            ))}
          </tr>
        </thead>
        <tbody>
          {dateGroups.map((dg,i) => (
            <tr key={`dg-${i}`}>
              {arrDays.map((day, atIndex) => (
                <td key={`${i}-${atIndex}`}>
                  <div 
                    className={`
                      date gn-layout align-center justify-center 
                      ${dg[atIndex] ? (dg[atIndex].isToday ? 'today' : '') : ''} 
                      ${hasEvent(dg[atIndex]) ? 'has-event' : ''}
                      ${isSelectedDate(dg[atIndex]) ? 'selected' : ''}
                    `}
                    onClick={() => {
                      if(hasEvent(dg[atIndex])){
                        this.selectDate(dg[atIndex].date);
                      }
                    }}
                  >
                    {dg[atIndex] ? dg[atIndex].date.date() : null}
                  </div>
                </td>
              ))}
            </tr>
          ))}
        </tbody>
      </table>
    )

    const Events = (
      <div className="events gn-margin-L margin-top gn-padding-L padding-top">
        <div className="gn-primary-color font-secondary-medium title-bordered">All Events</div>
        <Collapse 
          // defaultActiveKey={['0']} 
          bordered={false}
          className="gn-margin-L margin-top"
          onChange={ keys => this.setState({eventKeys: keys})}
        >
          {this.state.filteredEvents.map((event, i) => (
            <Panel 
              key={i}  
              className="event-panel gn-margin-N margin-bottom"
              header={
                <div className="panel-header gn-padding-N padding-all">
                    <div className="gn-layout align-center">
                      {this.state.eventKeys.indexOf(String(i)) < 0 ? (
                          <MediaImage
                            src={`${backendConf.static.host}${event.image}`}
                            alt={event.title}
                          />
                      ) : (null)}
                      <div className="title font-secondary gn-margin-N margin-left">{event.title}</div>
                    </div>
                </div>
              } 
            >
              <EventDetails 
                event={event} key={`event-${i}`} i={i}
                backendConf={backendConf}
              />
            </Panel>
          ))}
        </Collapse>
      </div>
    )

    return(
      <div className="bb-calendar gn-padding-L padding-all gn-margin-N margin-top">
        <h2 className="gn-primary-color font-secondary-medium title-bordered">
          Acara Bulan Ini
        </h2>
        {Control}
        {DateTable}
        {Events}
      </div>
    )
  }
}

export default connect( state => ({
  backendConf: state.backendConf
}))(Calendar);