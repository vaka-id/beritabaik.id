import React from 'react';
import { connect } from 'react-redux';
import { MediaImage} from '../Media';
import { ArticleAuthor, ArticleLink, ArticleActions, articleService } from '../Article';
import ContentPanel from '../ContentPanel'
import config from '../../server/config/environment';
import renderHTML from 'react-render-html';
import Footer from './__components/Footer';
import CommentBox from './__components/CommentBox';
import CommentList from './__components/CommentList';
import httpService from 'golibs/services/httpService';
import Swiper from 'react-id-swiper';
import qs from 'query-string';
import { Button, Row, Col, Card, Spin } from 'antd';
import YouTube from 'react-youtube';
import './ArticleDetails.scss';
import { isMobile } from 'react-device-detect';

const slideDuration = 6000;
const slideSpeed = 1000;

const injectLocation = (location, rawHtml) => {
  const tags = ['<p', '<div', '<section'];
  const getTopIndex = (htmlArr) => {
    let topIndex = 1;
    for(let n = 1 ; n < htmlArr.length ; n++){
      let arr = htmlArr[n].split('>');
      if(arr.length > 1 && arr[1].length > 0){
        topIndex = n;
        break;
      }
    }

    return topIndex;
  }
  if(location !== null){
    for(let i = 0 ; i < tags.length ; i++){
      let tag = tags[i];
      let rep = `${tag}<--rep--`;
      let htmlArr = rawHtml.replace(new RegExp(tag, 'g'), rep).split(tag);
      if(htmlArr.length > 1){
        htmlArr[0] = String(htmlArr[0]).replace(/<br \/>/g, '');
        let topIndex = getTopIndex(htmlArr);
        htmlArr[topIndex] = `<--rep--><span>${location.length > 0 ? '<strong>'+location+'</strong> - <span>' : ''}${htmlArr[topIndex].replace('<--rep--', '<span')}`;
        let result = String(htmlArr.toString()).replace(/,<--rep--/g, tag)
        return result;
        break;
      } else {
        if(i === tags.length - 1) return rawHtml;
      }
    }
  } else {
    return rawHtml
  }
}

const { getVideoId } = articleService;

class ArticleDetails extends React.Component{
  constructor(){
    super();
    this.swiper = null;
    this.state = {
      onProgress: false,
      isLoadingEditorPick:true,
      editorPicks:[]
    }
  }

  componentDidMount(){
    this.fetchArticleEditorPick();
    setTimeout(this.fireEventOnHover, 500);
  }

  fetchArticleEditorPick = async () => {
    const { host, baseUrl } = this.props.backendConf
    const { article } = this.props;
    if(article.article_type === 'video'){
      try{
        let res = await httpService.get(`${host}${baseUrl}/article-by-editor-pick?page=1&per_page=2&type=${article.article_type}`);
        this.setState({
          isLoadingEditorPick:false,
          editorPicks: res.data.data
        })
      }catch(error){
        this.setState({isLoadingEditorPick:false})
        console.warn(error)
      }
    }
  }

  fireEventOnHover = () => {
    let btns = document.getElementsByClassName('swiper-pagination-bullet');
    for(let i = 0 ; i < btns.length ; i++){
      let btn = btns[i];
      btn.onmouseover = () => {
        btn.click();
      }
    }
  }

  onNextArticle = async isPrevious => {
    const { host, baseUrl } = config.backend;
    const { article, query } = this.props;
    try{
      this.setState({onProgress:true})
      let res = await httpService.get(
        `${host}${baseUrl}/article-${isPrevious ? 'previous' : 'next'}/${article.editorial.slug}/${article.id}?${qs.stringify({
          editorialSlug:article.editorial.slug,
          articleId:article.id
        })}`
      )
      window.open(`/read?${qs.stringify({
        slug:res.data.slug,
        editorialSlug:article.editorial.slug
      })}`, '_self');
    }catch(error){
      message.error(`Gagal memuat artikel ${isPrevious ? 'sebelumnya' : 'selanjutnya'}, coba lagi!`);
    }
  }

  goNext = () => {
    if (this.swiper) {
      this.swiper.slideNext(slideSpeed)
      let { swiperIndex } = this.state;
      if(swiperIndex < this.props.images.length - 1){
        swiperIndex++;
        this.setState({swiperIndex});
      }
    }
  }

  goPrev = () => {
    if (this.swiper) {
      this.swiper.slidePrev(slideSpeed);
      let { swiperIndex } = this.state;
      if(swiperIndex > 0){
        swiperIndex--;
        this.setState({swiperIndex});
      }
    }
  }

  render(){
    const {article, images, videoHeight, backendConf} = this.props;
    const {onProgress, isLoadingEditorPick, editorPicks} = this.state;
    const isGallery = ['image', 'video', 'y-image'].indexOf(article.article_type) >= 0  ? true : false;
    const swipperParams = {
      slidesPerView: 'auto',
      centeredSlides: true,
      spaceBetween: 48,
      slidesPerGroup: 1,
      loop: true,
      grabCursor: true,
      lazy: true,
      loopFillGroupWithBlank: true,
      rebuildOnUpdate: false,
      autoplay: {
        delay: 6000,
        disableOnInteraction: false
      },
      pagination: {
        el: '.swiper-pagination',
        type: 'bullets',
        clickable: true
      }
    };

    const getComponentImage = () => {
      return article.article_type === 'video' ? (
        <div className="gn-padding-N padding-top padding-right padding-left" style={{background: 'white'}}>
          <YouTube
            videoId={getVideoId(article.sources_path)}
            opts={{
              height: isMobile ? 192 : videoHeight,
              width: '100%',
              playerVars: { 
                autoplay: 0
              }
            }}
          />
        </div>
      ) : (
        images ? (
          <div style={{position:'relative'}} className="gn-layout align-center">
            <div className="control left">
              <Button 
                onClick={this.goPrev}
                type="primary"
                shape="circle"  
                className="gn-parimary-background"
                style={{display: images.length > 1 ? 'inline' : 'none'}}
              >
                <i className="fas fa-chevron-left"/>
              </Button>
            </div>
            <Swiper {...swipperParams} ref={node => {if(node) this.swiper = node.swiper }}>
              {images.map((d, i) => (
                  <img 
                    key={i}
                    src={`${config.backend.static.host}${d.url}`}
                  />
              ))}
            </Swiper>
            <div className="control right">
              <Button 
                onClick={this.goNext}
                type="primary"
                shape="circle"  
                className="gn-parimary-background"
                style={{display: images.length > 1 ? 'inline' : 'none'}}
              >
                <i className="fas fa-chevron-right"/>
              </Button>
            </div>
          </div>
        ) : (article.main_image ? (
            <MediaImage 
              src={`${config.backend.static.host}${article.main_image}`}
            />
          ) : null
        )
      )
    }

    const ComponentDetails = (
      <div>
        {getComponentImage()}
        <div className="content">
          {['image', 'video'].indexOf(article.article_type) >= 0 ? (
            <ArticleActions article={article}/>
          ) : null}
          <h1 className="font-secondary">{article.title}</h1>
          {['image'].indexOf(article.article_type) < 0 ? (
            <ArticleAuthor 
              author={article.reporter_name}
              publishedAt={article.publish_date}
              fromNow
            />
          ) : null}
          {article !== "" && article.article_type !== 'video' && article.editorial.slug !== 'infografis' ? (
            <div className="body gn-margin-N margin-top">
              {renderHTML(
                isGallery ? (article.article_type === 'video' ? '' : article.content ) : injectLocation(
                  (article.city ? article.city.split(',')[0] : ""), 
                  article.content
                )
              )}
              {['berita-kamu'].indexOf(article.editorial.slug) >= 0 ? (
                <span 
                  style={{
                    color:'rgb(153,153,153)',
                    fontSize: 15
                  }}
                >
                  Tulisan ini adalah kiriman dari user, isi tulisan ini sepenuhnya menjadi tanggung jawab penulis.
                </span>
              ) : null }
            </div>
          ) : null}
          {['image'].indexOf(article.article_type) >= 0 ? (
            <div className="gn-margin-N margin-top">
              <ArticleAuthor 
                author={article.reporter_name}
                publishedAt={article.publish_date}
                fromNow
              />
            </div>
          ) : null}
          {['image', 'video'].indexOf(article.article_type) < 0 ? (
            <Footer 
              isGallery={isGallery} 
              article={article}
              backendConf={backendConf}
            />
          ) : null}
        </div>
        <CommentBox article={article}/>
        <div className="gn-layout justify-between article-nav gn-padding-N padding-top padding-bottom">
          <Button onClick={() => this.onNextArticle(true)} disabled={onProgress}>
            <i className="fas fa-chevron-left"/> 
            <span className="font-secondary">{`${isGallery ? 'Galeri' : 'Berita'} Sebelumnya`}</span>
          </Button>
          <Button onClick={() => this.onNextArticle()} disabled={onProgress}>
            <span className="font-secondary">{`${isGallery ? 'Galeri' : 'Berita'} Selanjutnya`}</span>
            <i className="fas fa-chevron-right"/> 
          </Button>
        </div>
      </div>
    )

    const EditorPicks = (
      <Card className="gn-margin-N margin-left editor-picks">
        <ContentPanel title="Video Pilihan" smallPadding/>
        { isLoadingEditorPick ? (
          <Spin spinning/>
        ) : (
          editorPicks.map((d, i) => (
            <div className="gn-margin-N margin-bottom" key={`article-editor-pick-${i}`}>
              <ArticleLink article={d}>
                <MediaImage
                  type={d.article_type}
                  src={d.article_type === 'video' ? `http://img.youtube.com/vi/${getVideoId(d.sources_path)}/1.jpg` : `${config.backend.static.host}${article.thumb_image}`}
                  alt={d.title}
                  label={d.editorial.name}
                  height={180}
                  style={{
                    height: 180,
                    width: '100%'
                  }}
                />
              </ArticleLink>
              <ArticleLink article={d}>
                <div className="gn-margin-NS margin-top margin-bottom">{d.title}</div>
              </ArticleLink>
            </div>
          ))
        )}
      </Card>
    )

    return (
      <div className="bb-article-details gn-padding-N padding-bottom">
        <div className="gn-grey-background">
          <div className="gn-container gn-padding-N padding-top">
            {article.article_type === 'video' ? (
              <Row type="flex">
                <Col lg={16} md={16} sm={24} xs={24}>
                  {ComponentDetails}
                </Col>
                <Col lg={8} md={8} sm={24} xs={24}>
                  {EditorPicks}
                </Col>
              </Row>
            ) : ComponentDetails}
          </div>
        </div>
        <CommentList article={article}/>
      </div>
    )
  }
}

ArticleDetails.defaultProps = {
  videoHeight: 565
}

export default connect( state => ({
  query:state.query,
  backendConf: state.backendConf
}))(ArticleDetails);