import React from 'react';
import httpService from 'golibs/services/httpService';
import config from '../../../../server/config/environment';
import qs from 'query-string';
import { MediaImage } from '../../../Media';
import { Pagination, Spin, message } from 'antd';
import moment from 'moment';
import renderHTML from 'react-render-html';
import { connect } from 'react-redux';
import './CommentList.scss';
import { setProperties } from 'redux/store';
import { bindActionCreators } from 'redux';

const CommentItem = ({d}) => (
  <div className="comment-item">
    <div className="gn-layout user-info align-center">
      <div className="avatar">
        <MediaImage
          style={{background:'transparent'}}
          src={d.user.image !== null ? `${config.backend.static.host}${d.user.image}` : '/static/images/no_avatar.png'}
        />
      </div>
      <div>
        <div className="name">{d.user.name}</div>
        <div className="date">
          {moment(moment(d.created_at).utc().format('YYYY-DD-MM HH:mm:ss'), 'YYYY-DD-MM HH:mm:ss').fromNow()}
        </div>
      </div>
    </div>
    <div className="comment gn-padding-M padding-top padding-bottom">
      {renderHTML(d.comments)}
    </div>
  </div>
)

class CommentList extends React.Component{
  constructor(){
    super();
    this.state = {
      total: 0,
      page: 1,
      pageSize: 5,
      onProgress:false
    }
  }

  componentDidMount(){
    this.fetchData();
  }

  fetchData = async (page) => {
    const { host, baseUrl } = config.backend;
    const { article } = this.props;
    const { pageSize } = this.state;

    page = page || this.state.page;
    this.setState({onProgress:true})

    try{
      let res = await httpService.get(
        `${host}${baseUrl}/article_comments/by_article/${article.id}?${
          qs.stringify({
            articleID: article.id,
            page,
            per_page: pageSize
          })}`
      )

      this.props.setProperties({
        articleComments:res.data.data
      })

      this.setState({
        total: res.data.total_entries_size,
        onProgress:false,
        page
      })
    }catch(error){
      message.error('Gagal memposting komentar, coba lagi!');
    }
  }

  render(){
    const { page, pageSize, total, onProgress } = this.state; 
    const { articleComments } = this.props;
    return(
      <div className="bb-article-details-comment-list gn-margin-L margin-top">
        <div className="gn-container">
          <div className="gn-padding-L padding-left padding-bottom padding-right gn-grey-background">
            { articleComments ? articleComments.map((d, i) => (
              <CommentItem d={d} key={`comment-item-${i}`}/>
            )) : null}
            <Spin spinning={onProgress} className="gn-margin-N margin-all"/>
            <div className="pagination-container gn-margin-L margin-top">
              { articleComments.length > 0 ? (
                <Pagination
                  current={page}
                  pageSize={pageSize}
                  total={total}
                  onChange={(page) => {
                    this.fetchData(page);
                  }}
                  itemRender={(current, type, originalElement) => {
                    if (type === 'prev') {
                      return <a><i className="fas fa-chevron-left"/></a>;
                    } if (type === 'next') {
                      return <a><i className="fas fa-chevron-right"/></a>;
                    }
                    return originalElement;
                  }}
                />
              ) : (
                <div className="gn-padding-N padding-top" style={{
                  display:onProgress ? 'none' : 'block'
                }}>
                  <i>Belum Ada Komentar</i>
                </div>
              )}
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default connect( state => ({
  store:state.store,
  articleComments:state.articleComments
}), dispatch => ({
  setProperties: bindActionCreators(setProperties, dispatch)
}))(CommentList);