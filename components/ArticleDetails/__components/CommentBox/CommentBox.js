import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { MediaImage } from '../../../Media';
import { Input, Button } from 'antd';
import './CommentBox.scss';
import httpService from 'golibs/services/httpService';
import config from '../../../../server/config/environment';
import cookies from 'react-cookies';
import { setProperties } from 'redux/store';
import { isMobile } from 'react-device-detect';
import _ from 'lodash';

const TextArea = Input.TextArea;

class CommentBox extends React.Component{
  constructor(){
    super();
    this.state = {
      onProgress:false,
      comments:''
    }
  }

  onSubmit = async (e) => {
    e.preventDefault();
    e.stopPropagation();
    const { host, baseUrl } = config.backend;
    const { user, article, articleComments } = this.props;

    if(user){
      try{
        this.setState({onProgress:true})
        let res = await httpService.post(
          `${host}${baseUrl}/auth/article_comments`,
          {
            article_id:article.id,
            active:true,
            comments:this.state.comments,
            likes_count:0,
            user_id: user.id
          }
        )

        this.setState({comments:'', onProgress:false})
        let _articleComments = _.clone(articleComments);
        let newComment = res.data;
        newComment.user = user;
        _articleComments.unshift(newComment);
        this.props.setProperties({
          articleComments:_articleComments
        })
      }catch(error){
        console.warn(error)
      }
    } else {
      this.gotoLoginPage();
    }
  }

  gotoLoginPage = () => {
    const { backendConf } = this.props;
    if(backendConf.loginPath){
      window.open(`${backendConf.loginPath}?redirectUri=${encodeURIComponent(window.location.href)}`, '_self');
    } else {
      window.open(`/login?reuri=${window.btoa(window.location.href)}`, '_self');
    }
  }

  render(){
    const { user } = this.props;
    const { onProgress } = this.state;

    const Avatar = (
      <div className="avatar">
          { user ? (
            <MediaImage
              style={{background:'transparent'}}
              src="/static/images/no_avatar.png"
            />
          ) : (
            <MediaImage
              style={{background:'transparent'}}
              src="/static/images/no_avatar.png"
            />
          )}
        </div>
    )

    const TextInput = (
      <form onSubmit={this.onSubmit}>
        <TextArea 
          rows={isMobile ? 3 : 4} 
          onChange={(e) => this.setState({comments:e.target.value})}
          value={this.state.comments}
          disabled={onProgress}
          required
          placeholder="Tulis Komentar"
        />
        <div className="action gn-margin-S margin-top">
          <Button htmlType="submit" disabled={onProgress}>Kirim</Button>
        </div>
      </form>
    )

    return(
      <div className="bb-article-details-comment-box gn-layout gn-padding-L padding-all gn-primary-background">
        {Avatar}
        {TextInput}
      </div>
    )
  }
}

export default connect(state => ({
  user:state.user,
  articleComments: state.articleComments,
  backendConf: state.backendConf
}), dispatch => ({
  setProperties:bindActionCreators(setProperties, dispatch)
}))(CommentBox);