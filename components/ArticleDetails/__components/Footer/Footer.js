import React from 'react';
import { MediaImage } from '../../../Media';
import { Button, Spin } from 'antd';
import './Footer.scss';
import { ArticleActions } from '../../../Article';
import { httpService } from 'golibs';
import _ from 'lodash';

class EditorInfo extends React.Component{
  constructor(){
    super();
    this.state = {
      editorData: null,
      isLoading: true
    }
  }
  componentDidMount(){
    this.fetchData();
  }

  fetchData = async() => {
    let { host, baseUrl } = this.props.backendConf;
    let { article } = this.props;
    try{
      let res = await httpService.get(`${host}${baseUrl}/article_authors/by_article/${article.id}?article_id=${article.id}`);
      this.setState({
        isLoading:false,
        editorData: _.find(res.data, {notes:"editor"})
      })
    }catch(error){
      this.setState({isLoading:false})
      console.warn(error)
    }
  }

  render(){
    let { backendConf } = this.props;
    let { editorData, isLoading } = this.state;
    return editorData ? (
      <div className="editor-info">
        { isLoading ? (
          <Spin spinning/>
        ) : (
          <div className="gn-layout align-center">
            <span>Editor :</span>        
            {editorData.user.image === null ? (
              <MediaImage
                className="gn-margin-N margin-left margin-right"
                src={`/static/images/no_avatar.png`}
              />
            ) : (
              <MediaImage
                className="gn-margin-N margin-left margin-right"
                src={`${backendConf.static.host}${editorData.user.image}`}
              />
            )}  
            <span>{editorData.user.name}</span>
            <a 
              href={`http://instagram.com/${editorData.user.instagram_username}`} 
              target="_blank"
              className="gn-margin-N margin-left">
              <Button>Follow</Button>
            </a>
          </div>
        )}
      </div>
    ) : null
  }
}

const Actions = () => (
  <div className="actions gn-layout align-center">
    <Button shape="circle">
      <i className="far fa-heart"/>
    </Button>
    <Button shape="circle">
      <i className="fab fa-facebook-f"/>
    </Button>
    <Button shape="circle">
      <i className="fab fa-twitter"/>
    </Button>
  </div>
)

const Footer = ({isGallery, article, backendConf}) => (
  <div className="bb-article-details-footer gn-layout align-center justify-between column-sm">
    {isGallery ? <span/> : (
      <EditorInfo 
        backendConf={backendConf}
        article={article}
      />
    )}
    <ArticleActions 
      article={article} 
      showMore
      expand
    />
  </div>
)

export default Footer;