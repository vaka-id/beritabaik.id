import React from 'react';

const Separator = ({isVerticalMode=false}) => (
  <div 
    className="bb-separator gn-margin-N margin-top margin-bottom"
    style={{
      width: isVerticalMode ? 1 : '100%',
      height: isVerticalMode ? '100%' : 1,
      backgroundColor: '#ddd'
    }}
  />
)

export default Separator;