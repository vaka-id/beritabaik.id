import React from 'react';
import Author from '../Author';
import Actions from '../Actions';
import { MediaImage } from '../../Media';
import { Card, Row, Col } from 'antd';
import config from '../../../server/config/environment';
import qs from 'query-string';
import YouTube from 'react-youtube';
import './Card.scss';
import ArticleLink from '../Link';
import service from '../service';
import Lazy from 'react-lazyload';

const verticalHeight = 80;
const horizontalHeight = 180;
const { getVideoId } = service;

const ACardImage = ({article, isVerticalMode}) => (
  <div>
    <ArticleLink article={article}>
      <MediaImage
        type={article.article_type}
        src={article.article_type === 'video' ? `http://img.youtube.com/vi/${getVideoId(article.sources_path)}/1.jpg` : `${config.backend.static.host}${article.thumb_image}`}
        alt={article.title}
        label={article.editorial.name}
        height={isVerticalMode ? verticalHeight : horizontalHeight}
        style={{
          height: isVerticalMode ? verticalHeight : horizontalHeight,
          width: '100%'
        }}
      />
    </ArticleLink>
  </div>
)

const ACardDefaultContent = ({article, showActions, isVerticalMode}) => (
  <div 
    className={`body ${isVerticalMode ? 'gn-layout column flex justify-between gn-margin-N margin-left' : 'gn-layout column justify-between'}`}
    style={{
      height: isVerticalMode ? verticalHeight : '100%'
    }}
  >
    <div className="gn-flex">
      {showActions ? (
        <Actions 
          className="gn-margin-S margin-bottom"
          article={article}
        />
      ) : null}
      <ArticleLink article={article}>
        <h3 className={`font-secondary-medium ${isVerticalMode ? '' : 'gn-margin-N margin-bottom'}`}>{article.title}</h3>
      </ArticleLink>
    </div>
    <Author 
      author={article.reporter_name}
      publishedAt={article.publish_date}
      fromNow
    />
  </div>
)

const ACard = ({article, isVerticalMode=false, showActions=true, style}) => (
  <div 
    className={`bb-article-card ${isVerticalMode ? 'vertical' : 'normal'}`}
    style={style}
  >
    <Card
      hoverable={false}
      cover={isVerticalMode ? null : (
        <ACardImage article={article} isVerticalMode={isVerticalMode}/>
      )}
      bordered={ !isVerticalMode }
    >
      { isVerticalMode ? (
        <Row type="flex">
          <Col span={9}>
            <ACardImage article={article} isVerticalMode={isVerticalMode}/>
          </Col>
          <Col span={15}>
            <ACardDefaultContent 
              article={article} 
              isVerticalMode={isVerticalMode}
              showActions={showActions}
            />
          </Col>
        </Row>
      ) : (
        <ACardDefaultContent 
          article={article} 
          isVerticalMode={isVerticalMode}
          showActions={showActions}
        />
      )}
    </Card>
  </div>
)

export default ACard;