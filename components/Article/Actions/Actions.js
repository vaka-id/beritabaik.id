import React from 'react';
import { connect } from 'react-redux';
import { Button, message, Menu, Dropdown, Modal, Radio, Input } from 'antd';
import './Actions.scss';
import httpService from 'golibs/services/httpService';
import qs from 'query-string';
import { FacebookShareButton, TwitterShareButton, WhatsappShareButton } from 'react-share';

const reportList = [
  'Spam, commercial, or advertising purpose',
  'Pornography',
  'Violent content',
  'Harmful or dangerous act',
  'Ethnicity, religion, race, inter-group relations',
  'Other'
]

class Actions extends React.Component{
  constructor(){
    super();
    this.state = {
      likeStatus: null,
      showReport:false,
      valueToReport:reportList[0],
      otherReport:"",
      onProgress: false
    }
  }

  componentDidMount(){
    this.fetchLikeDate();
  }

  fetchLikeDate = async () => {
    const { host, baseUrl } = this.props.backendConf;
    const { article, user } = this.props;
    if(article && user){
      try{
        let res = await httpService.get(`${host}${baseUrl}/auth/article-like-state/${article.id}/${user.id}`)
        let likeStatus = res.data;
        likeStatus.liked = likeStatus.liked === 'true' ? true : false;
        this.setState({
          likeStatus
        })
      }catch(error){
        console.log(error);
      }
    }
  }

  love = async () => {
    const { host, baseUrl } = this.props.backendConf;
    const { article, user } = this.props;
    if(user){
      try{
        let res = await httpService.post(
          `${host}${baseUrl}/article-like-unlike`,
          {
            liked:!this.state.likeStatus.liked,
            article_id: article.id,
            user_id: user.id
          }
        )
        this.setState({likeStatus:res.data})
      }catch(error){
        message.error('Gagal, mohon periksa koneksi jaringan anda')
      }
    }else{
      this.gotoLoginPage();
    }
  }

  report = async () => {
    const { host, baseUrl } = this.props.backendConf;
    const { article, user } = this.props;

    if(user){
      let { valueToReport, otherReport } = this.state;
      if(valueToReport === 'Other' && otherReport === ""){
        message.error('Harap mengisi kolom laporan');
      } else {
        this.setState({onProgress:true})
        try{
          await httpService.post(
            `${host}${baseUrl}/auth/article_reporteds`,
            {
              article_id: article.id,
              active: article.active,
              editorial_id:article.editorial_id,
              response: valueToReport === 'Other' ? otherReport : valueToReport,
              user_id:user.id
            }
          )
          this.setState({
            valueToReport: reportList[0],
            otherReport:'',
            showReport: false,
            onProgress: false
          })
          message.success('Laporan anda telah berhasil');
        }catch(error){
          console.log(error);
          this.setState({onProgress:false})
          message.error('Gagal report, mohon periksa koneksi jaringan anda')
        }
      }
    } else {
     this.gotoLoginPage();
    }
  }

  gotoLoginPage = () => {
    const { backendConf } = this.props;
    if(backendConf.loginPath){
      window.open(`${backendConf.loginPath}?redirectUri=${encodeURIComponent(window.location.href)}`, '_self');
    } else {
      window.open(`/login?reuri=${window.btoa(window.location.href)}`, '_self');
    }
  }

  render(){
    const { article, className, expand, webAddress, showMore } = this.props;
    const { likeStatus } = this.state;
    const articleAddress = `${webAddress}/read?${qs.stringify({
      editorialSlug:article.editorial.slug,
      slug: article.slug
    })}`

    const ReportModal = (
      <Modal
        title="Report"
        visible={this.state.showReport}
        footer={[
          <Button key="back" onClick={() => this.setState({showReport:false})}>Cancel</Button>,
          <Button key="submit" type="primary" disabled={this.state.onProgress} onClick={this.report}>
            Submit
          </Button>,
        ]}
      >
        <p>Why Reporting?</p>
        <Radio.Group 
          onChange={(e) => this.setState({valueToReport:e.target.value})}
          value={this.state.valueToReport}
        >
          {reportList.map((d, i) => (
            <Radio  
              value={d}
              key={`report-key-${i}`}
              style={{
                display:'block'
              }}
            >
              {d}
            </Radio>
          ))}
        </Radio.Group>
        {this.state.valueToReport === 'Other' ? (
          <div className="gn-margin-N margin-top">
            <Input.TextArea 
              rows={4} 
              required
              onChange={ e => this.setState({otherReport: e.target.value})}
              value={this.state.otherReport}
            />
          </div>
        ) : null}
      </Modal>
    )

    const FacebookShare = (
      <FacebookShareButton
        url={articleAddress}
        quote={article.title}
      >
        <Button shape="circle">
          <i className="fab fa-facebook-f"/>
        </Button>
      </FacebookShareButton>
    )

    const TwitterShare = (
       <TwitterShareButton
        url={articleAddress}
        title={article.title}
      >
        <Button shape="circle">
          <i className="fab fa-twitter"/>
        </Button>
      </TwitterShareButton>
    )

    const WhastappShare = (
      <WhatsappShareButton
        url={articleAddress}
        title={article.title}
      >
        <Button shape="circle">
          <i className="fab fa-whatsapp"/>
        </Button>
      </WhatsappShareButton>
    )

    return(
      <div className={`bb-article-actions gn-layout align-center ${expand ? 'expanded' : ''} ${className}`}>
        <Button shape="circle" onClick={this.love}>
          {likeStatus && likeStatus.liked ? <i className="fas fa-heart liked"/> : <i className="far fa-heart"/>}
        </Button>
        { expand ? (
          <div className="gn-layout expand">
            {FacebookShare}
            {TwitterShare}
            {WhastappShare}
            { showMore ? (
              <Dropdown 
                placement="topCenter"
                overlay={
                  <Menu>
                    <Menu.Item key="1" onClick={() => this.setState({showReport:true})}>
                      <i className="fas fa-exclamation-circle"/> Report
                    </Menu.Item>
                  </Menu>
                }>
                <Button shape="circle" className="more">
                  <i className="fas fa-ellipsis-h"/>
                </Button>              
              </Dropdown>
            ) : null}
          </div>
        ) : (
          <Dropdown 
            placement="topCenter"
            overlay={
              <Menu>
                <Menu.Item>
                  <div className="gn-layout">
                    {FacebookShare}
                    {TwitterShare}
                    {WhastappShare}
                  </div>
                </Menu.Item>
              </Menu>
            }>
              <Button shape="circle">
                <i className="fas fa-share-alt"/>
              </Button>
          </Dropdown>
        )}
        {ReportModal}
      </div>
    )
  }
}

Actions.defaultProps = {
  showMore: false
}

export default connect( state => ({
  backendConf:state.backendConf,
  webAddress:state.webAddress,
  user: state.user
}))(Actions);