import qs from 'query-string';

export default {
  getVideoId: source => {
    let parseUrl = qs.parseUrl(source).query;
    if(parseUrl.video_id){
      return parseUrl.video_id
    } else if(parseUrl.v){
      return parseUrl.v;
    } else {
      let splitUrl = source.split('youtu.be/');
      if(splitUrl.length > 1) return splitUrl[1];
      else ''
    }
  }
}