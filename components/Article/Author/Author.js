import React from 'react';
import moment from 'moment';
import './Author.scss';

const Author = ({author, publishedAt, fromNow=true, className=''}) => (
  <div className="bb-article-author font-secondary">
    <div className={`line gn-secondary-background ${className}`}/>
    {`${author} | ${fromNow ? moment(moment(publishedAt).utc().format('YYYY-DD-MM HH:mm:ss'), 'YYYY-DD-MM HH:mm:ss').fromNow() : moment(moment(publishedAt).utc().format('YYYY-DD-MM HH:mm:ss'), 'YYYY-DD-MM HH:mm:ss').format('lll')}`}
  </div>
)

export default Author;