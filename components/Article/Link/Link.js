import React from 'react';
import qs from 'query-string';

const ArticleLink = ({children, article, editorialSlug}) => (
  <a href={`/read?${qs.stringify({
      slug:article.slug,
      editorialSlug: editorialSlug ? editorialSlug : article.editorial.slug
    })}`}
    style={{
      color:'inherit'
    }}
  >
    {children}
  </a>
)

export default ArticleLink;