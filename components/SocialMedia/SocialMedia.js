import React from 'react';
import './SocialMedia.scss';

const media = [
  {
    name:'facebook', 
    iconClassName:'fab fa-facebook-f', 
    url:'https://www.facebook.com/beritabaik.id/'
  },
  {
    name:'twitter', 
    iconClassName:'fab fa-twitter', 
    url:'https://twitter.com/beritabaik_id'
  },
  {
    name:'instagram', 
    iconClassName:'fab fa-instagram', 
    url:'https://www.instagram.com/beritabaik.id/'
  },
  {
    name:'youtube', 
    iconClassName:'fab fa-youtube', 
    url:'https://www.youtube.com/channel/UCMhqsN7csDXMaJCIvt3s0BQ?view_as=subscriber'
  }
]

const SocialMedia = ({type, style, className}) => (
  <div className={`bb-social-media ${type} gn-layout ${className}`} style={style}>
    {media.map( d => (
      <div className="item gn-layout justify-center align-center" key={d.name}>
        <a href={d.url}>
          <i className={d.iconClassName}/>
        </a>
      </div>
    ))}
  </div>
)

SocialMedia.defaultProps = {
  type:'normal',
  style:{},
  className:''
}

export default SocialMedia;