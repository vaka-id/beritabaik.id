import React from 'react';
import './ContentPanel.scss';

const ContentPanel = ({title, moreLink, moreLabel, children, smallPadding, withContentBorder}) => (
  <div className={`bb-content-panel gn-padding-${smallPadding ? 'NS' : 'N'} padding-top padding-bottom`}>
    <div className="title font-secondary gn-margin-N margin-bottom">{title}</div>
    {children ? <div className={`content ${withContentBorder ? 'bordered' : ''}`}>{children}</div> : null }
    {moreLink ? (
      <div className="footer gn-layout align-center">
        <div>
          <a href={moreLink}>{moreLabel ? moreLabel : 'Buka lebih banyak lagi'}</a> 
        </div>
      </div>
    ) : null}
  </div>
)

export default ContentPanel;