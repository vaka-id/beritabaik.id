import React from 'react';
import ContentPanel from '../ContentPanel';
import { ArticleCard } from '../Article';
import Pagination from '../Pagination';
import { Col, Row, Spin } from 'antd';
import InfiniteScroll from 'react-infinite-scroller';
import _ from 'lodash';
import './ArticleList.scss';

const defaultState = () => ({
  data: [],
  hasMore: false,
  total:0,
  query:{
    page:1
  },
  isLoading: false,
  error:{
    isError:false,
    errorMessage:''
  }
})

class ArticleList extends React.Component{
  state = defaultState();

  componentDidMount(){
    this.fetchData( null, 'did mount');
  }

  fetchData = (newQuery, timana) =>  {
    const { fetchDataOnClient } = this.props;
    if(fetchDataOnClient){
      const { error, query, data } = this.state;
      let _query = _.merge(query, newQuery)
      this.setState({
        query:_query, 
        isLoading:true
      });
      fetchDataOnClient(_query, (newData, total, isError, errorMessage) => {
        if(isError){
          error.isError = true;
          error.errorMessage = errorMessage
        } else {
          const _data = this.state.data.concat(newData);
          this.setState({
            data : _data,
            total, isLoading:false,
            hasMore: _data.length < total ? true : false
          })
        }
      })
    }
  }

  onPagination = (page) => {
    if(this.props.fetchDataOnClient){
      this.fetchData({page})
    } else {
      this.props.onPagination({page})
    }
  }

  render(){
    const data = this.props.data || this.state.data;
    const { page, total, pageSize, articleCardStyle={} } = this.props;
    const { title, moreLink, moreLabel, isVerticalMode, showActions, className, fetchDataOnClient, hrefOption, defaultCol=12 } = this.props;
    const _Row = (
      <Row type="flex" key="_row">
        {data && data.length > 0 ? data.map((d, i) => (
          <Col 
            className="col" 
            key={`article-list-${i}`}
            lg={isVerticalMode ? 24 : defaultCol}
            md={isVerticalMode ? 24 : 16} 
            sm={24}
            xs={24} 
          >
            <ArticleCard 
              article={d} 
              isVerticalMode={isVerticalMode} 
              showActions={showActions}
              style={articleCardStyle}
            />
          </Col>
        )) : <span className="no-results"></span>}
      </Row>
    )

    return(
      <div className={`bb-article-list ${className}`}>
        <ContentPanel
          title={title}
          moreLink={moreLink}
          moreLabel={moreLabel}
          withContentBorder={isVerticalMode}
        >
          <div className={`content-container ${isVerticalMode ? 'vertical' : ''}`}>
            {fetchDataOnClient ? (
              <InfiniteScroll
                  pageStart={1}
                  initialLoad={false}
                  loadMore={(page) => this.fetchData({page}, 'infinite')}
                  hasMore={this.state.hasMore}
                  loader={<Spin spinning key="loader"/>}
                  threshold={64}
                  useWindow={true}
              >
                {_Row}
              </InfiniteScroll>
            ) : (
            <div>
              {_Row}
              <Pagination 
                total={total} 
                page={page}
                pageSize={pageSize}
                hrefOption={hrefOption}
              />
            </div>)}
          </div>
        </ContentPanel>
      </div>
    )
  }
}

ArticleList.defaultProps = {
  className:'',
  title:'', // CONTENT PANEL TITLE,
  moreLink:null, // CONTENT PANEL MORE LINK
  moreLabel:null, // CONTENT PANEL MORE LABEL
  fetchDataOnClient:null,
  showPagination:false,
  hrefOption: {
    url:'/',
    query:{}
  }, // PAGINATION OPTION 
  onPagination:null, // WILL BE IGNORED IF FETCH DATA ON CLIENT IS DEFINED
  data: null, // WILL BE IGNORED IF FETCH DATA ON CLIENT IS DEFINED
  isVerticalMode: null // CARD VIEW isVerticalMode ['VERTICAL']
}

export default ArticleList;