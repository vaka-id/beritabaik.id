import React from 'react';
import Lazy from 'react-lazyload';
import './Image.scss';

class Image extends React.Component{
  constructor(){
    super();
    this.state = {
      showImage: true,
      loaded: false
    }
  }
  render(){
    const { style, label, height, labelSize, type, ...imgProps} = this.props;
    return(
      <div className="bb-media-image" style={style}>
        {this.state.loaded ? null : (
          <div className="default">
            <i className="fas fa-image"/>
          </div>
        )}
        { this.state.showImage ? (
          <Lazy height={height}>
            <img 
              {...imgProps} 
              onError={() => this.setState({showImage:false})}
              onLoad={() => this.setState({loaded: true})}
            />
          </Lazy>
        ) : null}
        { label ? (
          <div className={`label gn-primary-background font-secondary ${labelSize}`}>
            {label}
          </div>
        ) : null}
        {type === 'video' ? (
          <div className="play-control gn-layout align-center justify-center">
            <i className="fab fa-youtube"/>
          </div>
        ) : null}
      </div>
    )
  }
}

export default Image;