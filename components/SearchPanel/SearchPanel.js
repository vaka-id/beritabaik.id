import React from 'react';
import connect from 'react-redux';
import { Input, Button, DatePicker, message } from 'antd';
import './SearchPanel.scss';
import qs from 'query-string';
import moment from 'moment';

const { RangePicker } = DatePicker;
const dateFormat = 'YYYY-MM-DD';
const defaultState = () => ({
  keyword:'',
  reporter_name:'',
  date: [null, null],
  isOnClientData:false,
  showOptions: false
})

class SearchPanel extends React.Component{
  constructor(){
    super();
    this.state = defaultState();
  }

  componentDidMount(){
    const { title, reporter_name, start_date, end_date } = this.props.defaultQuery;
    let _set = {keyword:title};
    if(reporter_name) _set.reporter_name = reporter_name;
    if(start_date && end_date ){
      _set.date = [moment(start_date), moment(end_date)];
    }
    if(start_date && end_date){
      _set.showOptions = true;
    }
    this.setState(_set);
  }

  changeDate = (val) => {
    this.setState({
      date:val
    })
  }

  onSearch = async (e, isAdvanced = false) => {
    e.preventDefault();
    e.stopPropagation();
    const { reporter_name, date} = this.state;
    let urlParameterQuery = {}
    if(isAdvanced){
      if(reporter_name) urlParameterQuery.reporter_name = reporter_name;
      if(date[0] && date[1]) {
        urlParameterQuery.start_date = date[0].format(dateFormat);
        urlParameterQuery.end_date = date[1].format(dateFormat);
      }
    } else {
     urlParameterQuery.title = this.state.keyword
    }
    
    window.open(`/search?${qs.stringify(urlParameterQuery)}`, '_self');


    // const { host, baseUrl } = this.props.backendConf;
    // this.setState({
    //   isOnClientData:true,
    //   isLoading: true
    // })
    // try{
    //   let res = await httpService.get(`${host}${baseUrl}/articles-search?${qs.stringify({
    //     article_type:'news',
    //     title:query.title,
    //     page:pquery.page || 1,
    //     per_page: 9
    //   })}`)
    // }catch(error){
    //   this.setState({isLoading:false})
    //   message.error(error.message);
    // }

  }

  reset = () => {
    let urlParameterQuery = defaultState();
    delete urlParameterQuery.showOptions;
    delete urlParameterQuery.keyword;
    this.setState(urlParameterQuery);
  }

  render(){
    const Options = (
      <form 
        className="options gn-padding-M padding-all gn-margin-N margin-top"
        onSubmit={(e) => this.onSearch(e, true)}
      >
        <div className="alert">
          <b>INFORMASI</b> Nama pengguna/penusi berisi <i>username</i> yang dipakai penulis terkait
        </div>
        <div className="gn-layout gn-margin-L margin-top">
          <div className="field">
            <label>Penulis</label>
            <Input
              onChange={ e => this.setState({reporter_name: e.target.value})}
              value={this.state.reporter_name}              
            />
          </div>
          <div className="field">
            <label style={{minWidth:140}}>Tanggal Publish</label>
            <RangePicker
              showToday={false}
              format={dateFormat}
              onChange={ val => this.changeDate(val)}
              value={this.state.date}
              required
            />
          </div>
        </div>
        <div className="actions">
          <Button onClick={this.reset}>Batal</Button>
          <Button htmlType="submit">Terapkan</Button>
        </div>
      </form>
    )

    return(
      <div className="bb-search-panel gn-padding-N padding-top padding-bottom">
        <form onSubmit={this.onSearch}>
          <div className="gn-layout">
            <h2 className="gn-margin-M margin-right">Pencarian</h2>
            <div className="gn-flex">
              <div className="keyword-field">
                <div>
                  <Input 
                    placeholder="Search" 
                    onChange={ e => this.setState({keyword: e.target.value})}
                    style={{width:'100%'}}
                    value={this.state.keyword}
                    required
                  />
                </div>
                <Button htmlType="submit">
                  <i className="fas fa-search"/>
                </Button>
              </div>
              <div className="info gn-layout align-center justify-between gn-margin-S margin-top">
                <div>{`${this.props.total} Artikel`}</div>
                <div className="toggle-option" onClick={() => {
                  this.setState({
                    showOptions: !this.state.showOptions
                  })
                }}>
                  <span>Opsi Lanjutan</span>
                  <i className={`fas fa-${this.state.showOptions ? 'minus' : 'plus'}`}/>
                </div>
              </div>
            </div>
          </div>
        </form>
        { this.state.showOptions ? Options : null}
      </div>
    )
  }
}

SearchPanel.defaultProps = {
  defaultKeyword:''
}

export default SearchPanel;