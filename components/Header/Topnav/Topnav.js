import React from 'react';
import './Topnav.scss';
import { SocialMedia } from 'components';

const Topnav = (props) => (
  <div className="bb-topnav gn-layout align-center gn-primary-background">
    <div className="gn-container gn-layout align-center justify-end">
      <ul>
        <li>
          <a href="/about-us">
            Tentang Kami
          </a>
        </li>        
        <li>
          <a href="mailto:admin@beritabaik.id">
            <i className="fas fa-envelope"></i>admin@beritabaik.id
          </a>
        </li>
        <li>
          <a href="/contact-us">
            Kerjasama
          </a>
        </li>
        <li className="social-media">
          <SocialMedia type="small" />
        </li>
      </ul>
    </div>
  </div>
)

export default Topnav;