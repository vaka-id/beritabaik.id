import React from 'react';
import Appbar from './Appbar';
import Topnav from './Topnav';

class Header extends React.Component{
  render(){
    const { version, showVersion } = this.props;
    return(
      <div className="bb-header">
        <Topnav/>
        <Appbar
          version={version}
          showVersion={showVersion}
        />
      </div>
    )
  }
}

Header.defaultProps = {
  version:'0.0.0',
  showVersion:false
}

export default Header;