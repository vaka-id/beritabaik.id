import React from 'react';
import { connect } from 'react-redux'; 
import { Input, Row, Col, Button } from 'antd';
import Searchbar from './__components/Searchbar';
import Profile from './__components/Profile';
import './Appbar.scss';

const BtnWrite = ({user, url, backendConf}) => (
  <Row>
    <Col md={24} sm={0} xs={0}>
      <a 
        href={user ? url.cms : `${backendConf.loginPath ? backendConf.loginPath : '/login'}?redirectUri=${new Buffer(url.cms).toString('base64')}`}
        target={user ? '_blank' : '_self'}
      >
        <Button type="primary" className="btn-write gn-margin-S margin-left">
          { user ? (
            <div>
              <i className="far fa-edit"/> CMS
            </div>
          ) : (
            <div>
              <i className="far fa-edit"/> TULIS BERITA BAIK
            </div>
          )}
        </Button>
      </a>
    </Col>
    <Col md={0} sm={24} xs={24}>
      <a 
          href={user ? url.cms : `${backendConf.loginPath ? backendConf.loginPath : '/login'}?redirectUri=${new Buffer(url.cms).toString('base64')}`}
          target={user ? '_blank' : '_self'}
        >
        <Button type="primary" className="btn-write sm gn-margin-S margin-left">
          <i className="far fa-edit"/>
        </Button>
      </a>
    </Col>
  </Row>
)

class Appbar extends React.Component{
  state = {
    showSmSearchbar:false
  }

  toggleSmSearchbar = () => {
    let {showSmSearchbar} = this.state;
    this.setState({
      showSmSearchbar:!showSmSearchbar
    })
  }

  render(){
    const { showVersion, version, user, url, backendConf } = this.props;
    return(
      <div className="bb-appbar">
        <div className="gn-container">
          <Row type="flex" align="center">
            <Col md={9} sm={12} xs={12}>
              <div>
                <a href="/">
                  <div className="brands">
                    <img src="/static/images/logo-berita-baik.png" alt="logo berita baik"/>
                    { showVersion ? <div className="badges gn-primary-background">{version}</div> : null }
                  </div>
                </a>
              </div>
            </Col>
            <Col md={15} sm={12} xs={12}>
              <div className="actions gn-layout align-center">
                <Searchbar
                  onSmClick={this.toggleSmSearchbar}
                />
                <BtnWrite 
                  user={user}
                  url={url}
                  backendConf={backendConf}
                />
                <Profile 
                  user={user}
                  url={url}
                />
              </div>
            </Col>
            <Col md={0} sm={24} xs={24}>
              <Searchbar 
                onSm={true} 
                visible={this.state.showSmSearchbar}
              />
            </Col>
          </Row>
        </div>
      </div>
    )
  }
}

Appbar.defaultProps = {
  showVersion:false,
  version:'0.1.0'
}

export default connect( state => ({
  user: state.user,
  url: state.url,
  backendConf: state.backendConf
}))(Appbar);