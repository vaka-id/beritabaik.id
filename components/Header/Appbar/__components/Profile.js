import React from 'react';
import { connect } from 'react-redux';
import { Col, Row, Button, Menu, Dropdown, message } from 'antd';
import { MediaImage } from '../../..';
import Router from 'next/router';
import httpService from 'golibs/services/httpService';
import { url } from 'inspector';

const style = {
  menuItem:{
    padding:'0 8px', 
    margin:0, 
    background: 'white important'
  }
}

class Profile extends React.Component{
  goToPage = (prefix) => {
    window.open(`${prefix}?reuri=${window.btoa(Router.pathname)}`, '_self');
  }

  gotoLoginPage = () => {
    const { backendConf } = this.props;
    if(backendConf.loginPath){
      window.open(`${backendConf.loginPath}?redirectUri=${encodeURIComponent(window.location.href)}`, '_self');
    } else {
      window.open(`/login?reuri=${window.btoa(window.location.href)}`, '_self');
    }
  }

  logout = async () => {
    try{
      await httpService.get('/auth/logout');
      window.open('/', '_self');
    }catch(error){
      message.error(error.message)
    }
  }

  render(){
    const { user, url } = this.props;
    
    return(
      <div className="profile">
        { user ? (
          <Dropdown
            overlay={
              <Menu>
                <Menu.Item style={style.menuItem}>
                  <a href={url.profile}>
                    <div className="menu-profile-item">
                      <i className="fas fa-user"/> Profil Saya
                    </div>
                  </a>
                </Menu.Item>
                <Menu.Item style={style.menuItem} 
                  onClick={this.logout}
                >
                  <div className="menu-profile-item no-border">
                    <i className="fas fa-power-off"/> Keluar
                  </div>
                </Menu.Item>
              </Menu>
            }
          >
            <div className="avatar">
              <MediaImage
                src="/static/images/no_avatar.png"
              />
            </div>
          </Dropdown>
        ) : (
          <Row>
            <Col md={24} sm={0} xs={0}>
              <a onClick={() => this.gotoLoginPage()} className="gn-primary-color font-secondary">LOG IN</a>
              /
              <a onClick={() => this.gotoLoginPage()} className="gn-primary-color font-secondary">SIGN UP</a>
            </Col>
            <Col md={0} sm={24} xs={24}>
              <a onClick={() => this.gotoLoginPage()} className="gn-primary-color font-secondary">
                <Button>
                  <i className="far fa-user"/>
                </Button>
              </a>
            </Col>
          </Row>
        )}
      </div>
    )
  }
}

Profile.defaultProps = {
  user:null
}

export default connect( state => ({
  backendConf: state.backendConf
}))(Profile);