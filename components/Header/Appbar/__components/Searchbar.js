import React from 'react';
import { Input, Col, Row, Button } from 'antd'; 

class Searchbar extends React.Component{
  constructor(){
    super();
    this.state = {
      keyword:''
    }
  }

  onSearch = (e) => {
    e.preventDefault();
    e.stopPropagation();
    window.open(`/search?title=${this.state.keyword}&page=1`, '_self');
  }

  

  render(){
    const { keyword } = this.state;
    const { onSm, onSmClick, visible } = this.props;
    const InputKeyword = (
      <Input 
        placeholder="Search" 
        suffix={
          <i className="fas fa-search"/>
        }
        onChange={ e => this.setState({keyword: e.target.value})}
        value={keyword}
        style={{width: '100%'}}
      />
    )
    return(
      <div 
        className={`searchbar gn-flex ${onSm ? 'sm' : ''}`}
        style={{
          display: visible ? 'block' : 'none'
        }}
      >
        <form onSubmit={this.onSearch}>
          { onSm ? InputKeyword : (
            <Row>
              <Col md={24} sm={0} xs={0}>
                {InputKeyword}
              </Col>
              <Col md={0} sm={24} xs={24}>
                <Button onClick={onSmClick}>
                  <i className="fas fa-search"/>
                </Button>
              </Col>
            </Row>
          )}
        </form>
      </div>
    )
  }
}

Searchbar.defaultProps = {
  onSm:false,
  visible: true,
  onSmClick:() => {}
}

export default Searchbar;