import React from 'react';
import { Menu, Row, Col, Button } from 'antd';
import SocialMedia from '../../SocialMedia';
import qs from 'query-string';
import './Navbar.scss';
import _ from 'lodash';
import * as Scroll from 'react-scroll';
import { isMobile, isDesktop } from 'react-device-detect';

const scroll = Scroll.animateScroll;

const SubMenu = Menu.SubMenu;
const MenuItemGroup = Menu.ItemGroup;
const generateParams = (editorialSlug, defaultParams={}) => (qs.stringify(_.merge(defaultParams, {
  editorialSlug, page:1
})))

const menus = [
  {
    label:'Indonesia Baik',
    url:`/kanal?${generateParams('indonesia-baik')}`
  },
  {
    label:'Indonesia Bangga',
    url:`/kanal?${generateParams('indonesia-bangga', {editorialType:'p'})}`,
    children:[
      {label:'Komunitas', url:`/kanal?${generateParams('komunitas')}`},
      {label:'Sosok Inspiratif',url:`/kanal?${generateParams('sosok-inspiratif')}`},
      {label:'Prestasi',url:`/kanal?${generateParams('prestasi')}`}
    ]
  },
  {
    label:'Indonesia Membangun',
    url:`/kanal?${generateParams('indonesia-membangun')}`
  },
  {
    label:'Melancong',
    url:`/kanal?${generateParams('melancong', {editorialType:'p'})}`,
    children:[
      {label:'Tempat Wisata', url:`/kanal?${generateParams('tempat-wisata')}`},
      {label:'Kuliner', url:`/kanal?${generateParams('kuliner')}`}
    ]
  },
  {
    label:'Teknologi',
    url:`/kanal?${generateParams('teknologi')}`
  },
  {
    label:'Panggung',
    url:`/kanal?${generateParams('panggung', {editorialType:'p'})}`,
    children:[
      {label:'Musik', url:`/kanal?${generateParams('musik')}`},
      {label:'Fashion', url:`/kanal?${generateParams('fashion')}`},
      {label:'Film', url:`/kanal?${generateParams('film')}`},
      {label:'Seni', url:`/kanal?${generateParams('seni')}`},
      {label:'Gaya Hidup', url:`/kanal?${generateParams('gaya-hidup')}`},
    ]
  },
  {
    label:'Citra',
    children:[
      {label:'Galeri Foto', url:`/kanal?${generateParams('gallery-foto')}`},
      {label:'Video', url:`/kanal?${generateParams('video')}`},
      {label:'Infografis', url:`/kanal?${generateParams('infografis')}`}
    ]
  },
  {
    label:'Berita Kamu', 
    url:`/kanal?${generateParams('berita-kamu')}`
  },
  {
    label:'More',
    children:[
      {label:'Acara', url:'/event'},
      {label:'Foto Kamu', url:`/kanal?${generateParams('foto-kamu')}`}
    ]
  }
]


class Navbar extends React.Component{
  state = {
    showMenu:false,
    isSticky:false
  }

  componentDidMount(){
    window.addEventListener('scroll', this.handleScroll)
  }

  handleScroll = () => {
    let sY = window.scrollY;
    let { isSticky } = this.state;
    if(sY > 128 && !isSticky){
      this.setState({isSticky:true})
    } 
    if(sY <= 128 && isSticky){
      this.setState({isSticky:false})
    }
  }

  toggleCollapseMenu = () => {
    let { showMenu } = this.state;
    this.setState({
      showMenu:!showMenu
    })
  }

  render(){
    const { showMenu, isSticky } = this.state;
    const renderMenu = menus.map((d) => {
      return d.children ? (
        <SubMenu
          className="bb-navbar-menu-sub gn-secondary-background font-secondary"
          title={
            <div>
              <a href={d.url}>
                <span className="font-secondary">{d.label}</span>
              </a>
              <i className="fas fa-sort-down gn-margin-S margin-left"/>
            </div>
          } 
          key={d.label}
        >
          {d.children.map( child => (
            <Menu.Item key={child.label} href={child.url}>
              <a href={child.url}>
                <div>{child.label}</div>
              </a>
            </Menu.Item>
          ))}
        </SubMenu>
      ) : (
      <Menu.Item key={d.label}>
        <a href={d.url}>
          <div className="font-secondary">{d.label}</div>
        </a>
      </Menu.Item>
    )})

    return(
      <div className="bb-navbar gn-secondary-background">
        <div className="gn-container gn-layout align-center row">
          <div className={`gn-layout justify-between align-center logo-container`} style={{width: isMobile ? '100%' : 'unset'}}>
            {isSticky ? (
              <div className="logo gn-margin-S margin-right margin-left" onClick={() => scroll.scrollTo(0)}>
                <img src="/static/images/pm-berita-baik-inverse.png" alt="logo berita baik"/>
              </div>
            ) : <span/>}
            <Button className="btn-collapse-menu mobile" onClick={this.toggleCollapseMenu}>
              {showMenu ? <i className="fas fa-times"/> : <i className="fas fa-bars"/>}
            </Button>
          </div>
          <Menu
            className="menu desktop gn-flex"
            mode="horizontal"
            current={menus[0].label}
            theme="dark"
          >
            {renderMenu}
          </Menu>
          { showMenu ? (
            <Menu
              className="menu mobile"
              mode="inline"
              current={menus[0].label}
              theme="dark"
            >
              {renderMenu}
            </Menu>
          ) : null}
          {/* {isMobile ? (
             <SocialMedia type="small" className={`${showMenu ? 'show' : 'hide'}`}/>
          ) : ( isSticky ? null : <SocialMedia type="small" className={`${showMenu ? 'show' : 'hide'}`}/> )} */}
        </div>
      </div>
    )
  }
}

export default Navbar;