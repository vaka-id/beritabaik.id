import React from 'react';
import './Footbar.scss';
import * as Scroll from 'react-scroll';

const scroll = Scroll.animateScroll;

class Footbar extends React.Component{
  render(){
    return(
      <div className="bb-footbar">
        <div className="gn-container gn-layout align-center justify-between">
          <div>Copyright © 2018 PT Warta Milenial Indonesia</div>
          <div>
            <li>
              <a onClick={() => scroll.scrollTo(0)}>Sitemap</a>
            </li>
            <li>
              <a onClick={() => scroll.scrollTo(0)}>Contact Us</a>
            </li>
          </div>
        </div>
      </div>
    )
  }
}

export default Footbar;