import React from 'react';
import Footnav from './Footnav';
import Footbar from './Footbar';

class Footer extends React.Component{
  render(){
    return(
      <div className="footer">
        <Footnav/>
        <Footbar/>
      </div>
    )
  }
}

export default Footer;