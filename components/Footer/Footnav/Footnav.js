import React from 'react';
import SocialMedia from '../../SocialMedia';
import { Row, Col } from 'antd';
import * as Scroll from 'react-scroll';
import './Footnav.scss';

const scroll = Scroll.animateScroll;

const navs = [
  {
    label:'Beranda',
    url:'/'
  },
  {
    label:'Tentang Kami',
    url:'/about-us'
  },
  {
    label:'Kontak',
    func:() => scroll.scrollTo(0)
  },
  {
    label:'Aturan',
    url:'/guidelines'
  },
  {
    label:'Privasi',
    url:'/guidelines'
  },
  {
    label:'Cara Menulis di Berita Baik',
    url:'/guidelines'
  },
  {
    label:'Pedoman',
    url:'/guidelines'
  }
]

class Footnav extends React.Component{
  render(){
    return(
      <div className="bb-footnav">
        <div className="gn-container">
          <Row type="flex" align="center" justify="center">
            <Col span={24}>
              <SocialMedia type="medium"/>
            </Col>
            <Col span={24}>
              <div className="gn-margin-N margin-top">
                {navs.map( d => (
                  <li className="nav-item" key={d.label}>
                    { d.url ? <a href={d.url}>{d.label}</a> : <a onClick={d.func}>{d.label}</a> }
                  </li>
                ))}
              </div>
            </Col>
          </Row>
        </div>
      </div>
    )
  }
}

export default Footnav;