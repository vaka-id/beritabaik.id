import React from 'react';
import { connect } from 'react-redux';
import { Row, Col, Input, Button, message } from 'antd';
import './SubscriptionPanel.scss';
import httpService from 'golibs/services/httpService';

const defaultFormData = () => ({
  name:'',
  email:''
})

class SubscriptionPanel extends React.Component{
  constructor(){
    super();
    this.state = {
      formData:defaultFormData()
    }
  }

  handleChangeForm = (key, value) => {
    let { formData } = this.state;
    formData[key] = value;
    this.setState({formData});
  }

  onSubmit = async (e) => {
    e.preventDefault();
    e.stopPropagation();
    try{
      const { host, baseUrl } = this.props.backendConf;
      await httpService.post(
        `${host}${baseUrl}/user_subscribers`,
        this.state.formData
      )
      message.success('Terima kasih, data berhasil kami simpan');
      this.setState({formData:defaultFormData()})
    }catch(error){
      message.error('Gagal, periksa koneksi jaringan anda')
    }
  }
  render(){
    return(
      <div className="bb-subscribe-panel gn-padding-L padding-top padding-bottom">
        <div className="gn-container">
            <Row type="flex" align="center">
              <Col lg={8} md={8} sm={24} xs={24}>
                <div className="label gn-layout align-center">
                  <i className="far fa-envelope"/>
                  <span>Selalu Perbaharui</span>
                  <span>beritabaik</span>
                </div>
              </Col>
              <Col lg={16} md={16} sm={24} xs={24}>
                <form onSubmit={this.onSubmit}>
                  <div className="gn-layout align-center column-sm align-start-sm">
                    <Input 
                      placeholder="Nama Anda"
                      onChange={ e => this.handleChangeForm('name', e.target.value)}
                      value={this.state.formData.name}
                      required
                    />
                    <Input 
                      placeholder="Surel Anda"
                      onChange={ e => this.handleChangeForm('email', e.target.value)}
                      value={this.state.formData.email}
                      required
                    />
                    <Button htmlType="submit">BERLANGGANAN</Button>
                  </div>
                </form>
              </Col>
            </Row>
        </div>
      </div>
    )
  }
}

export default connect( state => ({
  backendConf: state.backendConf
}))(SubscriptionPanel);