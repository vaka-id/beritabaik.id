import React from 'react';
import { Card, Input, Button, Checkbox, message } from 'antd';
import Oauth2 from '../Oauth2';
import config from '../../server/config/environment';
import './SignUpPanel.scss';
import httpService from 'golibs/services/httpService';

const defaultState = () => ({
  onProgress: false,
  formData:{
    name:'',
    email:'',
    password:'',
    confirmPassword:''
  }
})

class SignUpPanel extends React.Component{
  constructor(){
    super();
    this.state = defaultState();
  }

  formHandleChange = (key, value) => {
    let { formData } = this.state;
    formData[key] = value;
    this.setState({formData});
  }

  onFormSubmit = async e => {
    e.preventDefault();
    e.stopPropagation();
    
    const defState = defaultState();
    const { onProgress } = defState; 
    this.setState({
      onProgress
    })
    
    const { host, baseUrl } = config.backend;
    let { formData } = this.state;

    if(formData.password !== formData.confirmPassword){
      message.error('Konfirmasi password tidak sama');
      formData.confirmPassword = '';
      this.setState({formData})
    }else{
      try{
        this.setState({onProgress:true})
        let res = await httpService.post(`${host}/user/pre-signup`, formData)
        console.log(res.data);
      }catch(error){
        message.error('Gagal Daftar');
        this.setState({
          onProgress:false
        })
      }
    }
  }

  render(){
    const { formData, onProgress } = this.state;

    return(
      <div className="bb-sign-up-panel">
        <Card>
          <div className="brand">
            <img src="/static/images/logo-berita-baik-pm.png" alt="logo berita baik"/>
          </div>
          
          <form onSubmit={this.onFormSubmit}>
          Buat Akun Kamu Sekarang
             <Input
                placeholder="Nama"
                value={formData.name}
                onChange={ e => this.formHandleChange('name', e.target.value)}
                disabled={onProgress}
                
              />
              <Input
                placeholder="Alamat Surel"
                value={formData.email}
                onChange={ e => this.formHandleChange('email', e.target.value)}
                disabled={onProgress}
                type="email"
                
              />
              <Input
                placeholder="Password"
                value={formData.password}
                onChange={ e => this.formHandleChange('password', e.target.value)}
                disabled={onProgress}
                
                type="password"
                required
              />
              <Input
                placeholder="Konfirmasi Password"
                value={formData.confirmPassword}
                onChange={ e => this.formHandleChange('confirmPassword', e.target.value)}
                disabled={onProgress}
                
                type="password"
                required
              />
              <div className="action gn-margin-S margin-top gn-layout align-center justify-between">
                <Button className="font-primary" type="primary" htmlType="submit" disabled={onProgress}>
                  <span className="font-primary">Daftar</span>
                </Button>
            </div>
          </form>
          <div className="misc gn-margin-N margin-top">
            <a href="/login">Kembali ke login</a>
          </div>
          <Oauth2/>
        </Card>
      </div>
    )
  }
}

export default SignUpPanel;