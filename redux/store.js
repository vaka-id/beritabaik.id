import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunkMiddleware from 'redux-thunk';

const defaultState = () => ({
  showLogoutDialog:false,
  user:null,
  today: new Date(),
  query: {},
  articleComments:[]
})

export const actionTypes = {
  AUTH_LOGOUT:'AUTH_LOGOUT',
  SET_PROPERTIES:'SET_PROPERTIES',
  ARTICLE_COMMENTS_PUSH:'ARTICLE_COMMENTS_PUSH'
}

//REDUCER
export const reducer = (state = defaultState(), action) => {
  switch(action.type){
    case actionTypes.AUTH_LOGOUT:
    return Object.assign({}, state, {
      showLogoutDialog:action.show
    })

    case actionTypes.SET_PROPERTIES:
    return Object.assign({}, state, action.properties)

    case actionTypes.ARTICLE_COMMENTS_PUSH:
    state.articleComments.push(action.data)
    return {...state}

    default:
    return state
  }
}

// ACTIONS
export const logoutDialog = (show) => dispatch => {
  return dispatch({
    type:actionTypes.AUTH_LOGOUT, 
    show:show
  })
}

export const setProperties = (properties = {}) => dispatch => {
  return dispatch({
    type:actionTypes.SET_PROPERTIES,
    properties
  })
}

export const pushArticleComment = (data) => dispatch => {
  return dispatch({
    type:actionTypes.ARTICLE_COMMENTS_PUSH,
    data
  })
}

export function initializeStore (initialState = defaultState()) {
  return createStore(reducer, initialState, composeWithDevTools(applyMiddleware(thunkMiddleware)))
}