import axios from 'axios';
import fetch from 'isomorphic-unfetch';
import cookies from 'react-cookies';
import config from '../../server/config/environment';

const setHeaders = () => ({
    Authorization:`${cookies.load(config.cookie.token.name)}`,
    ApplicationID:config.backend.appId
})

const errorHandler = response => {  
  if(response.status === 401){
    window.open('/login', '_self'); 
  } else{
    var error = new Error(response);
    error.response = response;
    return Promise.reject(error);
  }
}

const handleError = error => {
  if (error.response) {
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx
      //console.log(error.response.data);
      //console.log(error.response.status);
      //console.log(error.response.headers);
      const responseData = error.response.data;
      error.message = responseData.message || responseData.error || responseData.errmsg || responseData;
      
    } else if (error.request) {
      // The request was made but no response was received
      // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
      // http.ClientRequest in node.js
      // console.log(error.request);
      error.message = error.statusText
    }

    return error
}

axios.interceptors.request.use(config => {
  config.headers = setHeaders();
  return config;
})


const httpService = {
  fetch: url => {
    return new Promise((resolve, reject) => {
      fetch(url, {
        // headers:setHeaders(),
        credentials:'include'
      })
        .then( response => {
          if(response.ok){
            return resolve(response)
          } else {
            return errorHandler(response);
          }
        })
    })
  },
  get: ( url ) => {
    return new Promise((resolve, reject) => {
      axios.get(url)
        .then(response => resolve(response))
        .catch( error => {
          let _error = handleError(error);
          reject(_error)
        })
    })
  },
  post: ( url, data ) => {
    return new Promise((resolve, reject) => {
      axios.post(url, data)
        .then(response => resolve(response))
        .catch( error => {
          let _error = handleError(error);
          reject(_error)
        })
    })
  },
  put: ( url, data ) => {
    return new Promise((resolve, reject) => {
      axios.put(url, data)
        .then(response => resolve(response))
        .catch( error => {
          let _error = handleError(error);
          reject(_error)
        })
    })
  },
  delete: ( url, data ) => {
    return new Promise((resolve, reject) => {
      axios.delete(url)
        .then(response => resolve(response))
        .catch( error => {
          let _error = handleError(error);
          reject(_error)
        })
    })
  },
  getBaseUrl:(req) => {
    const baseUrl = req ? `${req.protocol}://${req.get('Host')}` : '';
    return baseUrl;
  }
}

export default httpService;