'use strict';
const _ = require('lodash');

module.exports = {
  updateItemInArray: (array, selector, item, upsert) => {
    let _item = _.find(array, selector);
    if(_item) array.splice(array.indexOf(_item), 1, item);
    else if(upsert) array.unshift(item);
    return array;
  },
  deleteItemInArray: (array, selector, item) => {
    let _item = _.find(array, selector);
    if(_item) array.splice(array.indexOf(_item), 1);
    return array;
  },
  fileToFormData: (file, meta = {}, fileLabel='file') => {
    const data = new FormData();
    data.append(fileLabel, file);
    const keys = Object.keys(meta);
    for(let i = 0 ; i < keys.length ; i++){
      const key = keys[i];
      data.append(key, meta[key]);
    }
    return data;
  }
}