import ReactPixel from 'react-facebook-pixel';

const init = (fbPixelId, advanceMatching = {}, config = {debug:true, autoConfig:false}) => {
  ReactPixel.init(fbPixelId, advanceMatching, config);
}

const pageView = () => {
  setTimeout(() => () => {
    ReactPixel.pageView();
    // ReactPixel.fbq('track', 'PageView')
  })
}

const viewContent = (data) => {
  setTimeout(() =>  ReactPixel.track('ViewContent', data), 500)
}

export default {
  init, viewContent, pageView
}