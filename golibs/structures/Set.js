import React from 'react';
import Head from 'next/head';
import _ from 'lodash';

const defaultLocationId = 'ID';

const baseStructure = () => ({
	'@context':'http://schema.org',
})

const corporateLogo = {
	'@type':'ImageObject',
	url:'https://beritabaik.id/static/img/logo_berita_baik.3ff08ee.png'
}

const publisher = {
	'@type':'Organization',
	name:'beritabaik.id',
	logo:corporateLogo
}

export const setArticle = (
	headline, image = [], datePublished, dateModified, authorName, description
) => {
	let structureData = _.merge(baseStructure(), {
		'@type':'NewsArticle',
		'mainEntityOfPage':{
			'@type':'WebPage',
			'@id':defaultLocationId
		},
		headline, image, datePublished, dateModified, description,
		author:{
			'@type': 'Person',
			name: authorName
		},
		publisher
	})

	return structureData;
}

export const Set = ({ld, isMeta, ...metaProps}) => (
	<Head>
		{isMeta ? (
			<meta {...metaProps}/>
		) : (
       		<script type="application/ld+json" dangerouslySetInnerHTML={{__html:`${JSON.stringify(ld)}`}}></script>
		)}
	</Head>
);

export const SetMeta = (props) => (
	<Head>
		<meta {...props}/>
	</Head>
)

export const SetTitle = ({title}) => (
	<Head>
		<title>{title}</title>
	</Head>
)

export const SetCustom = (props) => (
	<Head>
		{props.children}
	</Head>
)