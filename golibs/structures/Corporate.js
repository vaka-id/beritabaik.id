import React from 'react';

let ld = {
    "@context": "https://schema.org",
    "@type": "Organization",
    "url": "https://beritabaik.id.com",
    "logo": "https://beritabaik.id/static/img/logo_berita_baik.3ff08ee.png",
    "sameAs": [
        "https://www.facebook.com/beritabaik.id/",
        "https://twitter.com/beritabaik_id",
        "https://www.instagram.com/beritabaik.id/",
        "https://www.youtube.com/channel/UCMhqsN7csDXMaJCIvt3s0BQ?view_as=subscriber"
    ],
    "contactPoint": [{
      "@type": "ContactPoint",
      "telephone": "+62-813-8037-7753",
      "contactType": "customer service",
      "areaServed": "ID"
    },{
      "@type": "ContactPoint",
      "telephone": "+62-813-8037-7753",
      "contactType": "sales"
    }]
  }

export default () => (<script type="application/ld+json" dangerouslySetInnerHTML={{__html:`${JSON.stringify(ld)}`}}></script>);
