import React, {Component} from 'react';

export default (AuthComponent, _getInitalProps) => {
  return class Authenticated extends Component {
    static async getInitialProps(ctx){
      let user = null;
      let _initialPropsData = null;
      let props = {}

      if(_getInitalProps){
        _initialPropsData =  await _getInitalProps(ctx);
      }

      if(ctx.req.user){
        user = {
          _id:ctx.req.user._id,
          name:ctx.req.user.name,
          email:ctx.req.user.email,
          phone:ctx.req.user.phone,
          role:ctx.req.user.role,
          idNumber:ctx.req.user.idNumber
        }
      }

      props.user = user;
      if(_initialPropsData) props.initial = _initialPropsData;

      return props
    }

    /*constructor(props){
      super(props)
      this.state = {
        isLoading: true
      }
    }

    componentDidMount(){
      console.log(this.props);
    }*/

    render(){
      return(
        <AuthComponent {...this.props} user={this.props.user}/>
      )
    }
  }
}

