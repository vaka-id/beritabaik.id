import React from 'react';
import { Head } from 'next/document';
import stylesheet from "styles/index.less";
import Corporate from './structures/Corporate';

// import staticStyle from '../static/css/index.css';
//* <style dangerouslySetInnerHTML={{ __html: stylesheet }} /> 

const Meta = ({title}) => (
  <Head>
    <title>{title}</title>
    <meta charSet="utf-8" />
    <meta name="viewport" content="initial-scale=1.0, width=device-width" />
    <meta name="google-site-verification" content="yrghX3PcQ1BsmX3NmRdwl4CCmLQ3qMtvxbMVk7vHLws" />
    <Corporate/>
    
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/solid.css"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/regular.css"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/brands.css"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/fontawesome.css"/>
    <link rel="stylesheet" href="/static/css/index.css" />
    <link rel="stylesheet" href="/static/iconfont/iconfont.css" />
    <link rel="stylesheet" href="/_next/static/style.css" />
    
    <link rel="mask-icon" href="/static/images/logo/website_icon.svg" color="black" />

    <link rel="apple-touch-icon" sizes="57x57" href="/static/favicon/apple-icon-57x57.png"/>
    <link rel="apple-touch-icon" sizes="60x60" href="/static/favicon/apple-icon-60x60.png"/>
    <link rel="apple-touch-icon" sizes="72x72" href="/static/favicon/apple-icon-72x72.png"/>
    <link rel="apple-touch-icon" sizes="76x76" href="/static/favicon/apple-icon-76x76.png"/>
    <link rel="apple-touch-icon" sizes="114x114" href="/static/favicon/apple-icon-114x114.png"/>
    <link rel="apple-touch-icon" sizes="120x120" href="/static/favicon/apple-icon-120x120.png"/>
    <link rel="apple-touch-icon" sizes="144x144" href="/static/favicon/apple-icon-144x144.png"/>
    <link rel="apple-touch-icon" sizes="152x152" href="/static/favicon/apple-icon-152x152.png"/>
    <link rel="apple-touch-icon" sizes="180x180" href="/static/favicon/apple-icon-180x180.png"/>
    <link rel="icon" type="image/png" sizes="192x192"  href="/static/favicon/android-icon-192x192.png"/>
    <link rel="icon" type="image/png" sizes="32x32" href="/static/favicon/favicon-32x32.png"/>
    <link rel="icon" type="image/png" sizes="96x96" href="/static/favicon/favicon-96x96.png"/>
    <link rel="icon" type="image/png" sizes="16x16" href="/static/favicon/favicon-16x16.png"/>
    {/* <link rel="manifest" href="/static/favicon/manifest.json"/> */}
    
    <link rel="shortcut icon" href="/static/images/favicon/favicon.ico" />
    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    {/* <ins data-revive-zoneid="1" data-revive-id="ef3931656b827192976175c6ec5445e5"></ins>
    <script async src="//156.67.220.197/revive/www/delivery/asyncjs.php"></script> */}
    <script dangerouslySetInnerHTML={{__html:`
      var feedify = feedify || {};
      window.feedify_options={fedify_url:"https://feedify.net/",pkey:"BNd70HHzAkI3BWU2DaJlct49ASOuOR6QxvLYlFZdfLD6me7m97C5BvHks3F8TeDwQpoP1KPUCB3juTv8aAMh3t8"};
      (function (window, document){
        function addScript( script_url ){
          var s = document.createElement('script');
          s.type = 'text/javascript';
          s.src = script_url;
          document.getElementsByTagName('head')[0].appendChild(s);
        }
        addScript('https://tpcf.feedify.net/uploads/settings/6299d42be9d81b8a45297db92f46732d.js?ts='+Math.random());
        addScript('https://cdn.feedify.net/getjs/feedbackembad-min-3.0.js');
      })(window, document);
    `}}>
    </script>
    <style>{stylesheet}</style>
  </Head>
);

export default Meta;
