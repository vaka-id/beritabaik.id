import Ga from 'react-ga';

const init = (gaTrackingId, options={
  debug: true,
  titleCase: false
}) => {
  Ga.initialize(gaTrackingId)
}

const event = (data) => {
  setTimeout(() => Ga.event(data), 500);
}

const pageView = (location) => {
  setTimeout(() => Ga.pageview(location), 500);
}

export default {
  init, event, pageView
}