import React from 'react';
import dynamic from 'next/dynamic';
const LoginPanel = dynamic(import('../components/LoginPanel'));
// import { LoginPanel } from '../components';
class Login extends React.Component{
  render(){
    return(
      <div className="bg-login gn-layout align-center justify-center">
        <LoginPanel/>
      </div>
    )
  }
}

export default Login;