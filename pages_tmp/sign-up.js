import React from 'react';

import { SignUpPanel } from '../components';
class SignUp extends React.Component{
  render(){
    return(
      <div className="bg-login gn-layout align-center justify-center">
        <SignUpPanel/>
      </div>
    )
  }
}

export default SignUp;